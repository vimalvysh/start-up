const data = {
    "skillTypes": [
        {
            "name": "Project Management",
            "id": "pm",
            "skills": [
                {
                    "name": "Performance Review",
                    "id": "skill1"
                },
                {
                    "name": "Project Planning",
                    "id": "skill2"
                },
                {
                    "name": "Task Management",
                    "id": "skill3"
                }
            ]
        },
        {
            "name": "Social Media Management",
            "id": "smm",
            "skills": [
                {
                    "name": "Blogging",
                    "id": "skill4"
                },
                {
                    "name": "Digital Media",
                    "id": "skill5"
                },
                {
                    "name": "Content Management Systems (CMS)",
                    "id": "skill6"
                }
            ]
        },
        {
            "name": "Technical Lead",
            "id": "tl",
            "skills": [
                {
                    "name": "Database Management",
                    "id": "skill7"
                },
                {
                    "name": "Accounting Software",
                    "id": "skill8"
                },
                {
                    "name": "Inventory Management",
                    "id": "skill9"
                }
            ]
        }
    ]
    };

    export default data;