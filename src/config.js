const config = {
    // basename: only at build time to set, and don't add '/' at end off BASENAME for breadcrumbs, also don't put only '/' use blank('') instead,
    // like '/berry-material-react/react/default'
    basename: "/stage",
    defaultPath: "/dashboard/default",
    fontFamily: `'Roboto', sans-serif`,
    borderRadius: 12,
    outlinedFilled: true,
    theme: "light", // light, dark
    presetColor: "default", // default, theme1, theme2, theme3, theme4, theme5, theme6
    // 'en' - English, 'fr' - French, 'ro' - Romanian, 'zh' - Chinese
    i18n: "en",
    rtlLayout: false,
    jwt: {
        secret: "SECRET-KEY",
        timeout: "1 days"
    },
    firebase: {
        apiKey: "AIzaSyBernKzdSojh_vWXBHt0aRhf5SC9VLChbM",
        authDomain: "berry-material-react.firebaseapp.com",
        projectId: "berry-material-react",
        storageBucket: "berry-material-react.appspot.com",
        messagingSenderId: "901111229354",
        appId: "1:901111229354:web:a5ae5aa95486297d69d9d3",
        measurementId: "G-MGJHSL8XW3"
    },
    auth0: {
        client_id: "HvYn25WaEHb7v5PBT7cTYe98XATStX3r",
        domain: "demo-localhost.us.auth0.com"
    }
};

export const designations = [
    "Administrative Assistant",
    "Executive Assistant",
    "Marketing Manager",
    "Customer Service Representative",
    "Nurse Practitioner",
    "Software Engineer",
    "Sales Manager",
    "Data Entry Clerk",
    "Office Assistant"
];

export const careers = [
    "accountant",
    "actor",
    "actuary",
    "adhesive bonding machine tender",
    "adjudicator",
    "administrative assistant",
    "administrative services manager",
    "adult education teacher",
    "advertising manager",
    "advertising sales agent",
    "aerobics instructor",
    "aerospace engineer",
    "aerospace engineering technician",
    "agent",
    "agricultural engineer",
    "agricultural equipment operator",
    "agricultural grader",
    "agricultural inspector",
    "agricultural manager",
    "agricultural sciences teacher",
    "agricultural sorter",
    "agricultural technician",
    "agricultural worker",
    "air conditioning installer",
    "air conditioning mechanic",
    "air traffic controller",
    "aircraft cargo handling supervisor",
    "aircraft mechanic",
    "aircraft service technician",
    "airline copilot",
    "airline pilot",
    "ambulance dispatcher",
    "ambulance driver",
    "amusement machine servicer",
    "anesthesiologist",
    "animal breeder",
    "animal control worker",
    "animal scientist",
    "animal trainer",
    "animator",
    "answering service operator",
    "anthropologist",
    "apparel patternmaker",
    "apparel worker",
    "arbitrator",
    "archeologist",
    "architect",
    "architectural drafter",
    "architectural manager",
    "archivist",
    "art director",
    "art teacher",
    "artist",
    "assembler",
    "astronomer",
    "athlete",
    "athletic trainer"
];

export const skills = [
    "HTML",

    "CSS",

    "CSS tools",
    "Photoshop",
    "JavaScript",

    "Java",

    "JavaFX",
    "NodeJS",

    "Go",

    "Python",
    "NumPy",

    "Scrum",
    "Coaching",
    "Machine Learning",
    "UI/UX Design",
    "React",
    "Search Engine Optimization",
    "Project Coordination"
];

export default config;
