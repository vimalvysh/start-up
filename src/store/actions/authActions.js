import axiosInstance from "../services/api.service";
import {
    GENERATE_TOKEN_REQUEST,
    GENERATE_TOKEN_SUCCESS,
    GENERATE_TOKEN_FAIL,
    VALIDATE_USER_REQUEST,
    VALIDATE_USER_SUCCESS,
    VALIDATE_USER_FAIL
} from "../types/auth.types";

import {decodeToken} from "../../views/pages/authentication/authentication3/Register";

//#1: generateToken
export const generateTokenCreator = () => async (dispatch) => {
    localStorage.removeItem("access_token");
    dispatch({
        type: GENERATE_TOKEN_REQUEST
    });

    try {
        const {data} = await axiosInstance.get(`/auth/generateToken`);
        dispatch({type: GENERATE_TOKEN_SUCCESS, payload: data});
        console.log("data.access_token", data.access_token);
        localStorage.setItem("access_token", data.access_token);
        let userDecodeTokenInfo = decodeToken("user-token", data.access_token);
    } catch (error) {
        dispatch({type: GENERATE_TOKEN_FAIL, payload: error.message});
    }
};

//#2: validateUser
export const validateUserCreator = () => async (dispatch) => {
    dispatch({
        type: VALIDATE_USER_REQUEST
    });

    try {
        const {data} = await axiosInstance.get(`/auth/validateUser`);
        dispatch({type: VALIDATE_USER_SUCCESS, payload: data});
    } catch (error) {
        dispatch({type: VALIDATE_USER_FAIL, payload: error.message});
    }
};
