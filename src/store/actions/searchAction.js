import {SEARCH_REQUESTED, SHOW_SEARCH} from "../types/search.types";

// search value setting to redux.
export const searchRequested = (value) => async (dispatch) => {
    try {
        dispatch({type: SEARCH_REQUESTED, payload: value});
    } catch (error) {
        console.log(error);
    }
};

// To show search.
export const showSearch = (isShow) => async (dispatch) => {
    try {
        dispatch({type: SHOW_SEARCH, payload: isShow});
    } catch (error) {
        console.log(error);
    }
};
