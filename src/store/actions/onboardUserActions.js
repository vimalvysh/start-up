import axiosInstance from "../services/api.service";
import {    
  ONBOARD_UPDATE_USER_REQUEST,
  ONBOARD_UPDATE_USER_SUCCESS,
  ONBOARD_UPDATE_USER_FAIL
} from '../types/onboarduser.types';


//#1: Register 
export const onboardUpdateUserCreator = (onboardUpdateReq) => async (dispatch) => {
  dispatch({ type: ONBOARD_UPDATE_USER_REQUEST });
  try {
    const { data } = await axiosInstance.put(`/user/${onboardUpdateReq.userId}`, onboardUpdateReq);
    dispatch({ type: ONBOARD_UPDATE_USER_SUCCESS, payload: data });
    localStorage.setItem('onboard-update-info', JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: ONBOARD_UPDATE_USER_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

//Signout
export const signoutCreator = () => (dispatch) => {
  localStorage.removeItem('userInfo');  
  dispatch({ type: USER_SIGNOUT });
  document.location.href = '/signin';
};