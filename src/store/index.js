import {createStore, compose, applyMiddleware, combineReducers} from "redux";
import {logger} from "redux-logger";
import thunk from "redux-thunk";

import {persistStore} from "redux-persist";
import reducer from "./reducer";

// ===========================|| REDUX - MAIN STORE ||=========================== //
const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        trace: true,
        traceLimit: 1000
    }) || compose;

const middlewareList = [thunk];

// const middlewareList = [thunk, logger];
const initialState = {};

const enhancer = composeEnhancers(applyMiddleware(...middlewareList));

const store = createStore(reducer, initialState, enhancer);
const persister = persistStore(store);

export {store, persister};
