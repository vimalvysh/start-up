import {combineReducers} from "redux";
import {persistReducer} from "redux-persist";
import storage from "redux-persist/lib/storage";

// reducer import
import {generateTokeReducer, validateUserReducer} from "./reducers/authReducers";
import {onboardUpdateUserReducer} from "./reducers/onboardUserReducers";
import customizationReducer from "./customizationReducer";
import snackbarReducer from "./snackbarReducer";
import cartReducer from "./cartReducer";
import searchReducer from "./reducers/searchReducer";

// ===========================|| COMBINE REDUCER ||=========================== //

const reducer = combineReducers({
    generateTokenInfo: generateTokeReducer,
    validateUserInfo: validateUserReducer,
    onboardUpdateUserInfo: onboardUpdateUserReducer,
    customization: customizationReducer,
    snackbar: snackbarReducer,
    search: searchReducer,
    cart: persistReducer(
        {
            key: "cart",
            storage,
            keyPrefix: "berry-"
        },
        cartReducer
    )
});

export default reducer;
