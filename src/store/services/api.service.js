import axios from "axios";

// Default config options
const defaultOptions = {
    baseURL: "http://20.74.171.246/cv/api/",
    headers: {
        "Content-Type": "application/json"
    }
};

let instance = axios.create(defaultOptions);

// Set the AUTH token for any request
instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem("access_token");
    if (token) {
        config.headers.Authorization = token ? `Bearer ${token}` : "";
    } else {
        config.headers["x-access-token"] = localStorage.getItem("id_token");
    }
    return config;
});

export default instance;
