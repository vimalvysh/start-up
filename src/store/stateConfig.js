import {createStore, compose, applyMiddleware, combineReducers} from "redux";
import {logger} from "redux-logger";
import thunk from "redux-thunk";

//Native imports
import {generateTokeReducer, validateUserReducer} from "./reducers/authReducers";
import {onboardUpdateUserReducer} from "./reducers/onboardUserReducers";

const rootReducer = combineReducers({
    generateTokenInfo: generateTokeReducer,
    validateUserInfo: validateUserReducer,
    onboardUpdateUserInfo: onboardUpdateUserReducer
});

const initialState = {};
const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        trace: true,
        traceLimit: 1000
    }) || compose;

// const middlewareList = [thunk, logger];
const middlewareList = [thunk];

const enhancer = composeEnhancers(applyMiddleware(...middlewareList));

// const stateConfig = createStore(rootReducer, initialState, enhancer);

// export default stateConfig;
