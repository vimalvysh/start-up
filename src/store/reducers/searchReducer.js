import {SEARCH_REQUESTED, SHOW_SEARCH} from "../types/search.types";

const initialState = {
    isSearch: false,
    searchValue: ""
};

const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_REQUESTED:
            return {
                ...state,
                searchValue: action.payload
            };

        case SHOW_SEARCH:
            return {
                ...state,
                isSearch: action.payload
            };

        default:
            return state;
    }
};

export default searchReducer;
