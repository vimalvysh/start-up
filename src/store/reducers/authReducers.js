import {
    GENERATE_TOKEN_REQUEST,
    GENERATE_TOKEN_SUCCESS,
    GENERATE_TOKEN_FAIL,
    VALIDATE_USER_REQUEST,
    VALIDATE_USER_SUCCESS,
    VALIDATE_USER_FAIL
} from "../types/auth.types";

//#1: generateTokeReducer
export const generateTokeReducer = (state = {loading: true, info: {}}, action) => {
    switch (action.type) {
        case GENERATE_TOKEN_REQUEST:
            return {loading: true};

        case GENERATE_TOKEN_SUCCESS:
            return {
                loading: false,
                data: action.payload
            };
        case GENERATE_TOKEN_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
};

//#2: validateUserReducer
export const validateUserReducer = (state = {loading: true, info: {}}, action) => {
    switch (action.type) {
        case VALIDATE_USER_REQUEST:
            return {loading: true};

        case VALIDATE_USER_SUCCESS:
            return {
                loading: false,
                info: action.payload
            };
        case VALIDATE_USER_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
};
