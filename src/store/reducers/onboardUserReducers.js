import {    
  ONBOARD_UPDATE_USER_REQUEST,
  ONBOARD_UPDATE_USER_SUCCESS,
  ONBOARD_UPDATE_USER_FAIL
} from '../types/onboarduser.types';

//#1: onboardUpdateUserReducer
export const onboardUpdateUserReducer = (state = { loading: true }, action) => {
  switch (action.type) {
    case ONBOARD_UPDATE_USER_REQUEST:
      return { loading: true };
    case ONBOARD_UPDATE_USER_SUCCESS:
      return { loading: false, onboardUpdateInfo: action.payload };
    case ONBOARD_UPDATE_USER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};