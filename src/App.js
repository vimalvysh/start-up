import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";

import {ThemeProvider} from "@material-ui/core/styles";
import {CssBaseline, StyledEngineProvider} from "@material-ui/core";

// Active directory

import {AuthenticatedTemplate, UnauthenticatedTemplate, useMsal} from "@azure/msal-react";
import ProfileContent from "./ProfileContent";

// routing
import Routes from "routes";

// defaultTheme
import themes from "themes";

// project imports
import Locales from "ui-component/Locales";
import NavigationScroll from "layout/NavigationScroll";
import RTLLayout from "ui-component/RTLLayout";
import Snackbar from "ui-component/extended/Snackbar";

// ===========================|| APP ||=========================== //

const App = () => {
    const customization = useSelector((state) => state.customization);

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={themes(customization)}>
                <CssBaseline />
                {/* RTL layout */}
                <RTLLayout>
                    <Locales>
                        <NavigationScroll>
                            {/* <AuthenticatedTemplate>
                                <p>You are signed in!</p>
                                <ProfileContent />
                            </AuthenticatedTemplate>
                            <UnauthenticatedTemplate>
                                <p>You are not signed in! Please sign in.</p>
                            </UnauthenticatedTemplate> */}
                            <Routes />
                            <Snackbar />
                        </NavigationScroll>
                    </Locales>
                </RTLLayout>
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

export default App;
