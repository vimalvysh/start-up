export const msalConfig = {   
    auth: {
        instance: 'https://jusoorlms.b2clogin.com/', 
        tenant: 'jusoorlms.onmicrosoft.com',
        signInPolicy: 'B2C_1_useronboarding',
        clientId:'93bb452a-a519-4d36-93db-29909b0ad2d4',
        cacheLocation: 'sessionStorage',
        scopes: ['https://jusoorlms.onmicrosoft.com/93bb452a-a519-4d36-93db-29909b0ad2d4/Read.User'],
        redirectUri: 'http://localhost:3000',
        postLogoutRedirectUri: window.location.origin        
      },
    cache: {
        cacheLocation: 'sessionStorage', // This configures where your cache will be stored
        storeAuthStateInCookie: false // Set this to "true" if you are having issues on IE11 or Edge
    }
};

// Add scopes here for ID token to be used at Microsoft identity platform endpoints.
export const loginRequest = {
    scopes: ['User.Read']
};

// Add the endpoints here for Microsoft Graph API services you'd like to use.
export const graphConfig = {
    graphMeEndpoint: 'https://graph.microsoft.com/v1.0/me'
};



