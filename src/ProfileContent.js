import React, { useState } from "react";
import { useMsal } from "@azure/msal-react";
import { Button, Grid, Paper, Box, styled } from '@material-ui/core';
import { loginRequest } from "./authConfig";
import { callMsGraph } from "./graph";
import ProfileData from './ProfileData';
import { Link } from 'react-router-dom';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

function ProfileContent(props) {
    const { instance, accounts } = useMsal();
    const [graphData, setGraphData] = useState(null);

    const name = accounts[0] && accounts[0].name;

    function RequestProfileData() {
        const request = {
            ...loginRequest,
            account: accounts[0]
        };

        // Silently acquires an access token which is then attached to a request for Microsoft Graph data
        instance.acquireTokenSilent(request).then((response) => {
            callMsGraph(response.accessToken).then(response => setGraphData(response));
            console.log('#graphData:', graphData);
        }).catch((e) => {
            instance.acquireTokenPopup(request).then((response) => {
                callMsGraph(response.accessToken).then(response => setGraphData(response));
            });
        });
    }

    
    return (
        <>
            <h5 className="card-title">Welcome {name}</h5>
            <Grid container spacing={2}>                
                <Grid item xs={2}>
                    <Item><Button component={Link} to="/onboard/onboardstep1" variant="contained">Do Onboarding</Button></Item>
                </Grid>
                <Grid item xs={3}>
                    {graphData ? 
                        <ProfileData graphData={graphData} />
                        : 
                        <Item><Button variant="outlined" onClick={RequestProfileData}>Request Profile Information</Button> </Item>               
                    }
                </Grid>
            </Grid>
         
          
        </>
    );
};


export default ProfileContent;