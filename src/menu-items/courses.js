import React from 'react';

// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconClipboardCheck, IconNotebook, IconBook, IconPhone, IconQuestionMark, IconPictureInPicture, IconMessages, IconForms, IconBorderAll, IconChartDots, IconStairsUp } from '@tabler/icons';

// constant
const icons = { IconClipboardCheck, IconNotebook,IconBook,IconPhone, IconQuestionMark,IconPictureInPicture, IconMessages, IconForms, IconBorderAll, IconChartDots, IconStairsUp };

// ===========================|| UI FORMS MENU ITEMS ||=========================== //

const courses = {
    id: 'courses',
    title: <FormattedMessage id="courses" />,
    type: 'group',
    children: [
        {
            id: 'All Courses',
            title: <FormattedMessage id="All Courses" />,
            type: 'item',
            icon: icons.IconNotebook,
            url: '/app/allcourses'
        },
        {
            id: 'My Courses',
            title: <FormattedMessage id="My Courses" />,
            type: 'collapse',
            icon: icons.IconBook,
            children: [
                {
                    id: 'Bootstrap',
                    title: <FormattedMessage id="Bootstrap" />,
                    type: 'item',
                    url: '/components/bootstrap',
                    breadcrumbs: false
                },
                {
                    id: 'Datatable',
                    title: <FormattedMessage id="Datatable" />,
                    type: 'item',
                    url: '/components/datatable',
                    breadcrumbs: false
                }               
            ]
        },       
        {
            id: 'FAQ',
            title: <FormattedMessage id="FAQ" />,
            type: 'item',
            url: '/chart/faq',
            icon: icons.IconQuestionMark
        },
        {
            id: 'Contact Support',
            title: <FormattedMessage id="Contact Support" />,
            type: 'item',
            url: '/forms/contact_support',
            icon: icons.IconPhone
        }
    ]
};

export default courses;
