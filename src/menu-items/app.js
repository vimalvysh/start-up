import React from "react";

// third-party
import {FormattedMessage} from "react-intl";

// assets
import {IconTable, IconDirections, IconId, IconCalendarEvent, IconPhonePlus} from "@tabler/icons";

// constant
const icons = {IconTable, IconDirections, IconId, IconCalendarEvent, IconPhonePlus};

// ===========================|| DASHBOARD MENU ITEMS ||=========================== //

const dashboard = {
    id: "app",
    type: "group",
    children: [
        {
            id: "default",
            title: <FormattedMessage id="default" />,
            type: "item",
            url: "/profile",
            icon: icons.IconTable,
            breadcrumbs: false
        },
        {
            id: "jobs",
            title: <FormattedMessage id="jobs" />,
            type: "item",
            url: "/jobs",
            icon: icons.IconDirections,
            breadcrumbs: false
        }
    ]
};

export default dashboard;
