import React from 'react';

// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconBasket, IconDashboard , IconTrack, IconSchool, IconMessage, IconFlag3,IconUser, IconUserCheck, IconUsers, IconCertificate, IconMessages, IconMail, IconCalendar, IconNfc } from '@tabler/icons';

// constant
const icons = { IconUserCheck, IconDashboard, IconTrack, IconSchool,IconMessage,IconFlag3,IconUsers,IconBasket, IconUsers,IconCertificate, IconMessages, IconMail, IconCalendar, IconNfc };

// ===========================|| APPLICATION MENU ITEMS ||=========================== //

const application = {
    id: 'mentor',
    title: <FormattedMessage id="mentor" />,
    type: 'group',
    children: [
        {
            id: 'dashboard',
            title: <FormattedMessage id="dashboard" />,
            type: 'item',
            icon: icons.IconDashboard,
            url: '/app/dashboard'
        },
        {
            id: 'My Career Path',
            title: <FormattedMessage id="My Career Path" />,
            type: 'item',
            icon: icons.IconTrack,
            url: '/app/careerpath'
        },
        {
            id: 'View Aspirants',
            title: <FormattedMessage id="View Aspirants" />,
            type: 'item',
            url: '/app/viewaspirants',
            icon: icons.IconSchool,
            breadcrumbs: false
        },
        {
            id: 'Calendar',
            title: <FormattedMessage id="Calendar" />,
            type: 'item',
            icon: icons.IconCalendar,
            url: '/app/calendar'
        },
        {
            id: 'Messages',
            title: <FormattedMessage id="Messages" />,
            type: 'item',
            icon: icons.IconMessage,
            url: '/app/messages'
        },
        {
            id: 'Goals',
            title: <FormattedMessage id="Goals" />,
            type: 'item',
            url: '/app/goals',
            icon: icons.IconFlag3,
            breadcrumbs: false
        },
        {
            id: 'Meetings',
            title: <FormattedMessage id="Meetings" />,
            type: 'item',
            icon: icons.IconUsers,
            url: '/app/meetings'
        },
        {
            id: 'Certificates',
            title: <FormattedMessage id="Certificates" />,
            type: 'item',
            url: '/app/certificates',
            icon: icons.IconCertificate,
            breadcrumbs: false
        }
    ]
};

export default application;
