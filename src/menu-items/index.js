import application from "./application";
import courses from "./courses";

import dashboard from "./dashboard";
import widget from "./widget";
import forms from "./courses";
import elements from "./elements";
import pages from "./pages";
import utilities from "./utilities";
import support from "./support";
import other from "./other";
import app from "./app";

// ===========================|| MENU ITEMS ||=========================== //

const menuItems = {
    // items: [dashboard, widget, application, forms, elements, pages, utilities, support, other]
    items: [app]
};

export default menuItems;
