import React from 'react';


const ProfileData = (props) => {
    return (
        <div id="profile-div">           
            <p>
                <strong>User Name: </strong> {props.graphData.displayName}
            </p>
            <p>
                <strong>User Principal Name: </strong> {props.graphData.userPrincipalName}
            </p>
            <p>
                <strong>Email: </strong> {props.graphData.mail}
            </p>
            <p>
                <strong>Id: </strong> {props.graphData.id}
            </p>
        </div>
    );
};

export default ProfileData;




