import * as React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {   
    Typography,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Card,
    CardContent,
    Chip
} from "@material-ui/core";

// Icons - @material-ui
import WorkIcon from '@material-ui/icons/Work';
import BookIcon from '@material-ui/icons/Book';
import PersonIcon from '@material-ui/icons/Person';
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import SchoolIcon from '@material-ui/icons/School';

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: '100%',
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: '#fff'
    },
    stripStyle : {
        marginBottom: '10px',
        backgroundColor: '#E3F2FD',
        borderRadius: '8px',
    },
    selected : {
        backgroundColor: '#EDE7F6',
    }
}));

export default function CareerStory_Step1() {
    const classes = useStyles();
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    Let’s Start with your Career Story
                </Typography>

                <div className={classes.root}>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem className={classes.stripStyle } button selected={selectedIndex === 0} onClick={(event) => handleListItemClick(event, 0)}>
                            <ListItemIcon>
                                <WorkIcon />
                            </ListItemIcon>
                            <ListItemText primary="I am a Working Professional" />
                            <Chip label="01" color="primary" size="small" />
                        </ListItem>

                        <ListItem className={classes.stripStyle} button selected={selectedIndex === 1} onClick={(event) => handleListItemClick(event, 1)}>
                            <ListItemIcon>
                                <BookIcon />
                            </ListItemIcon>
                            <ListItemText primary="Just a College Student" />
                            <Chip label="02" color="primary" size="small" />
                        </ListItem>
                        <ListItem className={classes.stripStyle} button selected={selectedIndex === 2} onClick={(event) => handleListItemClick(event, 2)}>
                            <ListItemIcon>
                                <SchoolIcon />
                            </ListItemIcon>
                            <ListItemText primary="Completed my Graduation" />
                            <Chip label="03" color="primary" size="small" />
                        </ListItem>

                        <ListItem className={classes.stripStyle} button selected={selectedIndex === 3} onClick={(event) => handleListItemClick(event, 3)}>
                            <ListItemIcon>
                                <LocalMallIcon />
                            </ListItemIcon>
                            <ListItemText primary="I am a Working as an Intern" />
                            <Chip label="04" color="primary" size="small" />
                        </ListItem>
                        <ListItem className={classes.stripStyle} button selected={selectedIndex === 4} onClick={(event) => handleListItemClick(event, 4)}>
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="I am a business Owner" />
                            <Chip label="05" color="primary" size="small" />
                        </ListItem>
                        <ListItem className={classes.stripStyle} button selected={selectedIndex === 5} onClick={(event) => handleListItemClick(event, 5)}>
                            <ListItemIcon>
                                <FlightTakeoffIcon />
                            </ListItemIcon>
                            <ListItemText primary="We are a Startup" />
                            <Chip label="06" color="primary" size="small" />
                        </ListItem>
                    </List>
                </div>
            </CardContent>
        </Card>
    );
}
