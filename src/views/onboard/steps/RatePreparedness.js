import * as React from "react";
import {useDispatch} from "react-redux";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Grid, Typography, Card, CardContent, Alert} from "@material-ui/core";
import AnimateButton from "ui-component/extended/AnimateButton";
import MainCard from "ui-component/cards/MainCard";
import {gridSpacing} from "store/constant";

// Icons - MUI

import StarBorderIcon from "@mui/icons-material/StarBorder";

import {onboardUpdateUserCreator} from "store/actions/onboardUserActions";

// Components
import UIMultiselect from "views/ui-elements/basic/UIMultiselect";
import NavigationButtons from "../components/NavigationButtons";

const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px",
        width: "100%"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    buttonNext: {
        textAlign: "right"
    },
    alertContainer: {
        backgroundColor: "#F2F7F9",
        color: "#828282",
        padding: "14px",
        borderRadius: "4px",
        marginTop: "4px",
        "& h5": {
            color: "#828282"
        }
    },

    star: {
        color: "#FFC107"
    }
}));

const RatinngItem = ({classes, title, rating}) => {
    return (
        <Grid container spacing={0} className={classes.alertContainer} justifyContent="space-between">
            <Grid item>
                <Typography variant="h5">{title}</Typography>
            </Grid>
            <Grid item>
                <StarBorderIcon className={rating > 1 ? classes.star : ""} />
                <StarBorderIcon className={rating > 2 ? classes.star : ""} />
                <StarBorderIcon className={rating > 3 ? classes.star : ""} />
                <StarBorderIcon className={rating > 4 ? classes.star : ""} />
                <StarBorderIcon className={rating > 5 ? classes.star : ""} />
            </Grid>
        </Grid>
    );
};

export default function RatePreparedness({nextCallback, previousCallback}) {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [progress, setProgress] = React.useState(30);

    const [yearsOfExperience, setYearsOfExperience] = React.useState(0);
    let onboardUpdateReq = {};
    const userToken = JSON.parse(localStorage.getItem("user-token"));
    let year = 1;

    const setState = (event, year) => {
        setYearsOfExperience(year);
    };
    const handleExperienceClick = (event) => {
        nextCallback(event);

        onboardUpdateReq = {
            lastStep: userToken.microserviceFields.LAST_STEP + 1,
            status: userToken.microserviceFields.STATUS,
            userId: userToken.microserviceFields.USER_ID,
            mentor_yearsOfExperience: yearsOfExperience
        };
        dispatch(onboardUpdateUserCreator(onboardUpdateReq));
    };

    const takeBack = (event) => {
        previousCallback(event);
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} className={classes.aboutContainer}>
                        <Grid item xs={12}>
                            <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                                How do you rate your preparedness?
                            </Typography>
                            <RatinngItem classes={classes} title={"Hard Skills"} rating={4} />
                            <RatinngItem classes={classes} title={"Resume Preparedness"} rating={2} />
                            <RatinngItem classes={classes} title={"Company Research"} rating={5} />
                        </Grid>
                    </Grid>
                </Grid>
            </CardContent>
            <NavigationButtons nextCallback={nextCallback} previousCallback={previousCallback} />
        </Card>
    );
}
