import * as React from "react";
import {skills} from "../../../config";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Grid, Typography, Card, CardContent, Alert} from "@material-ui/core";
import AnimateButton from "ui-component/extended/AnimateButton";
import MainCard from "ui-component/cards/MainCard";
import {gridSpacing} from "store/constant";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import Snackbar from "@mui/material/Snackbar";

// Icons - MUI

import stateConfig from "../../../store/stateConfig";
import StarBorderIcon from "@mui/icons-material/StarBorder";

// Components
import UIMultiselect from "views/ui-elements/basic/UIMultiselect";
import NavigationButtons from "../components/NavigationButtons";
import useNotifications from "../components/useNotifications";

// services.
import {addSkill} from "../services";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 2.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};
const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px",
        width: "100%"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    buttonNext: {
        textAlign: "right"
    },
    alertContainer: {
        backgroundColor: "#F2F7F9",
        color: "#828282",
        padding: "14px",
        borderRadius: "4px",
        marginTop: "4px"
    },
    star: {
        color: "#FFC107"
    }
}));

export default function KeySkillsSelect({nextCallback, previousCallback}) {
    const classes = useStyles();
    const USER = JSON.parse(localStorage.getItem("user-token"));
    let USER_ID = USER.id;
    const [skillSet, setSkillSet] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
    const {isNotification, message, ShowSnackBar} = useNotifications();

    const handleSkillSet = (e) => {
        try {
            let lastSelected = e.target.value[e.target.value.length - 1];
            let skill = {
                name: lastSelected,
                skill_id: skillSet.length
            };
            setSkillSet((prevState) => [...prevState, skill]);
            setSelected(e.target.value);
        } catch (error) {
            console.log(error);
        }
    };

    const handleSelectLevel = (value) => {
        try {
            console.log(value);
        } catch (error) {
            console.log(error);
        }
    };

    const handleNextClick = () => {
        try {
            // When clicked next, then check is values there.
            if (skillSet.length > 0) {
                // if value the add to DB & next step.
                handleSubmit();
            } else {
                // else show validation.
                ShowSnackBar("Please select atleast one skill!");
            }
        } catch (error) {
            console.log(error);
        }
    };

    // On click next submit the form.
    const handleSubmit = async () => {
        try {
            // Validation.
            if (skillSet.length === 0) {
                ShowSnackBar("Please select atleast one skill");
                return;
            } else {
                let tempData = {
                    skill_id: skillSet[0].skill_id,
                    name: skillSet[0].name
                };
                let response = await addSkill(USER_ID, tempData);
                // if status = 200 then, call nextstep.
                if (response.status === 200) {
                    ShowSnackBar("Successful");
                    nextCallback();
                    return;
                }
                // else show notification.
                ShowSnackBar("Error in adding designation!");
            }
            // Call the api service.
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Card className={classes.root}>
            <Snackbar open={isNotification} autoHideDuration={6000} onClose={() => ShowSnackBar(null)}>
                <Alert onClose={() => ShowSnackBar(null)} severity="info" sx={{width: "100%"}}>
                    {message}
                </Alert>
            </Snackbar>
            <CardContent>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} className={classes.aboutContainer}>
                        <Grid item xs={12}>
                            <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                                Let us know about your key skills and how you rate them on a scale of 1 to 5
                            </Typography>

                            <UIMultiselect selected={selected} onChange={handleSkillSet}>
                                {skills.map((name) => (
                                    <MenuItem key={name} value={name}>
                                        {name}
                                    </MenuItem>
                                ))}
                            </UIMultiselect>
                            {skillSet.map((el) => (
                                <Grid key={el.id} container spacing={0} className={classes.alertContainer} justifyContent="space-between">
                                    <Grid item>
                                        <Typography variant="p">{el.name}</Typography>
                                    </Grid>
                                    <Grid item>
                                        <StarBorderIcon onClick={() => handleSelectLevel(1)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(2)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(3)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(4)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(5)} className={el.level > 1 ? classes.star : ""} />
                                    </Grid>
                                </Grid>
                            ))}
                        </Grid>
                        <NavigationButtons nextCallback={handleNextClick} previousCallback={previousCallback} />
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
}
