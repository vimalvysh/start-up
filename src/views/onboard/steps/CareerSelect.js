import * as React from "react";
import {careers} from "../../../config";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {
    Checkbox,
    Button,
    FormControlLabel,
    Grid,
    Typography,
    TextField,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Card,
    CardContent,
    Chip
} from "@material-ui/core";

import {gridSpacing} from "store/constant";
import MenuItem from "@mui/material/MenuItem";

// Icons - MUI
import StarBorderIcon from "@mui/icons-material/StarBorder";

import stateConfig from "../../../store/stateConfig";
import {onboardUpdateUserCreator} from "store/actions/onboardUserActions";

// Components
import UIMultiselect from "views/ui-elements/basic/UIMultiselect";
import NavigationButtons from "../components/NavigationButtons";

const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px",
        width: "100%"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    buttonNext: {
        textAlign: "right"
    }
}));

export default function CareerSelect({nextCallback, previousCallback}) {
    const classes = useStyles();
    const [progress, setProgress] = React.useState(15);
    const [careerList, setCareer] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
    const handleChangeCareer = (e) => {
        try {
            console.log("e", e.target.value.length);
            let lastSelected = e.target.value[e.target.value.length - 1];
            let selectedOption = {
                skill: lastSelected,
                id: careerList.length
            };
            setCareer((prevState) => [...prevState, selectedOption]);
            setSelected(e.target.value);
        } catch (error) {
            console.log(error);
        }
    };

    const handleSelectLevel = (value) => {
        try {
            console.log(value);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} className={classes.aboutContainer}>
                        <Grid item xs={12}>
                            <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                                Let us know where do you aspire to land your career. We’ll help to get you there
                            </Typography>
                            <UIMultiselect selected={selected} onChange={handleChangeCareer}>
                                {careers.map((name) => (
                                    <MenuItem key={name} value={name}>
                                        {name}
                                    </MenuItem>
                                ))}
                            </UIMultiselect>

                            {/* {careers.map((el, i) => (
                                <Grid key={i} container spacing={0} className={classes.alertContainer} justifyContent="space-between">
                                    <Grid item>
                                        <Typography variant="p">{el.skill}</Typography>
                                    </Grid>
                                    <Grid item>
                                        <StarBorderIcon onClick={() => handleSelectLevel(1)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(2)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(3)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(4)} className={el.level > 1 ? classes.star : ""} />
                                        <StarBorderIcon onClick={() => handleSelectLevel(5)} className={el.level > 1 ? classes.star : ""} />
                                    </Grid>
                                </Grid>
                            ))} */}
                        </Grid>
                    </Grid>
                </Grid>
                <NavigationButtons nextCallback={nextCallback} previousCallback={previousCallback} />
            </CardContent>
        </Card>
    );
}
