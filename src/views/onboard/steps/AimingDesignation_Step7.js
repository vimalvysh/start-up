import * as React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import AnimateButton from "ui-component/extended/AnimateButton";
import {
    Box,
    Button,
    Grid,
    Typography,
    FormControl,
    InputLabel,   
    Card,
    CardContent,    
    Select,
    MenuItem,
    FormHelperText,
} from "@material-ui/core";

// third party
import {Formik} from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: '100%',
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: '#fff'
    },
    stripStyle : {
        marginBottom: '10px',
        backgroundColor: '#E3F2FD',
        borderRadius: '8px',
    },
    selected : {
        backgroundColor: '#EDE7F6',
    }
}));

export default function AimingDesignation_Step7({...others}) {
    const classes = useStyles();
    const [progress, setProgress] = React.useState(20);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    Perfect! Tell us what designation are you aiming for?
                </Typography>

                <Formik
                    initialValues={{
                        jobProfileType: "",
                        submit: null
                    }}
                    validationSchema={Yup.object().shape({
                        jobProfileType: Yup.string().required("Job profile type is required")
                    })}
                    onSubmit={async (values, {setErrors, setStatus, setSubmitting}) => {
                        try {
                            // await firebaseEmailPasswordSignIn(values.email, values.password);
                            console.log("Signup data: ", values);

                            if (scriptedRef.current) {
                                setStatus({success: true});
                                setSubmitting(false);
                            }
                        } catch (err) {
                            console.error(err);
                            if (scriptedRef.current) {
                                setStatus({success: false});
                                setErrors({submit: err.message});
                                setSubmitting(false);
                            }
                        }
                    }}
                >
                    {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values}) => (
                        <form noValidate onSubmit={handleSubmit} {...others}>
                            <Grid container spacing={3}>
                            <Grid item xs={12}>
                                    <FormControl fullWidth sx={{minWidth: 120}}>
                                        <InputLabel id="designation-select">Designation Name</InputLabel>
                                        <Select
                                            labelId="designation-select"
                                            id="designation"
                                            name="designation"
                                            defaultValue={values.designation}
                                            onChange={handleChange}
                                            label="Designation Name"
                                        >
                                            <MenuItem value="">
                                                <em>Designation Name</em>
                                            </MenuItem>
                                            <MenuItem value={10}>Sr. Web Developer</MenuItem>
                                            <MenuItem value={20}>Business Lead</MenuItem>
                                            <MenuItem value={30}>Backend Developer</MenuItem>
                                        </Select>
                                        {errors.designation && (
                                            <FormHelperText error id="standard-weight-helper-text-designation">
                                                {" "}
                                                {errors.designation}{" "}
                                            </FormHelperText>
                                        )}
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    {errors.submit && (
                                        <Box
                                            sx={{
                                                mt: 3
                                            }}
                                        >
                                            <FormHelperText error>{errors.submit}</FormHelperText>
                                        </Box>
                                    )}

                                    <Box>
                                        <AnimateButton>
                                            <Button
                                                disableElevation
                                                disabled={isSubmitting}
                                                fullWidth
                                                size="large"
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                            >
                                                Submit
                                            </Button>
                                        </AnimateButton>
                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    );
}
