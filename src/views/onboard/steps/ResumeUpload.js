import * as React from "react";
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
// material-ui
import {makeStyles} from "@material-ui/styles";
import AnimateButton from "ui-component/extended/AnimateButton";

import {Box, Button, Typography, Alert, Card, CardContent, Grid, TextField, FormControl, FormHelperText} from "@material-ui/core";
import Snackbar from "@mui/material/Snackbar";

// icons.

// Redux.
import {validateUserCreator} from "../../../store/actions/authActions";

// Services.
import {updateProfile} from "../services";

// Components
import NavigationButtons from "../components/NavigationButtons";
import useNotifications from "../components/useNotifications";

// third party
import {Formik, Form} from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: "100%",
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: "#fff"
    },
    stripStyle: {
        marginBottom: "10px",
        backgroundColor: "#E3F2FD",
        borderRadius: "8px"
    },
    selected: {
        backgroundColor: "#EDE7F6"
    },
    chipStyle: {
        width: "100%",
        padding: "24px 0",
        borderRadius: "12px"
    }
}));

const initialFormValues = {
    intro: ""
};

export default function ResumeUpload({...others}) {
    const classes = useStyles();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {info} = useSelector((state) => state.validateUserInfo);
    console.log("info", info);
    const {isNotification, message, ShowSnackBar} = useNotifications();
    const USER = JSON.parse(localStorage.getItem("user-token"));
    let USER_ID = USER.id;
    // State to store intro.
    const [formValues, setFormVaules] = React.useState(initialFormValues);

    React.useEffect(() => {
        dispatch(validateUserCreator());
    }, []);

    // handleOnchange to store intro.
    const handleFormValueSet = (e) => {
        try {
            setFormVaules({
                ...formValues,
                [e.target.name]: e.target.value
            });
        } catch (error) {
            console.log(error);
        }
    };

    const handleFormValidatiion = () => {
        try {
            let intro = formValues?.intro.length ?? 0;
            if (intro === 0) {
                return false;
            }
            return true;
        } catch (error) {
            console.log(error);
        }
    };

    // handleSubmit to submit the form.
    const handleSubmit = async () => {
        try {
            // validation.
            if (!handleFormValidatiion()) {
                ShowSnackBar("Please add introduction!");
                return;
            }
            let tempData = {
                gender: info?.gender ?? "M",
                intro: formValues?.intro ?? "",
                is_graduate: info?.is_graduate ?? false,
                is_intern: info?.is_intern ?? false,
                interest: info?.interest ?? 0,
                total_experience: info?.interest ?? 0,
                preferred_companies: info?.preferred_companies ?? "",
                preferred_designations: info?.preferred_designations ?? {}
            };
            // true : add to DB.
            let response = await updateProfile(USER_ID, tempData);
            // if status = 200 then, call nextstep.
            if (response.status === 200) {
                ShowSnackBar("Successful");
                // redirect to user profile.
                navigate("/user-profile");

                return;
            }

            // false : show notification.
            ShowSnackBar("Error in adding designation!");
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Card className={classes.root}>
            <Snackbar open={isNotification} autoHideDuration={6000} onClose={() => ShowSnackBar(null)}>
                <Alert onClose={() => ShowSnackBar(null)} severity="info" sx={{width: "100%"}}>
                    {message}
                </Alert>
            </Snackbar>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    Let’s complete your career synopsis. Can your provide us a small introduction about you and upload your Resume?
                </Typography>

                <Formik
                    initialValues={{
                        jobProfileType: "",
                        submit: null
                    }}
                    validationSchema={Yup.object().shape({
                        jobProfileType: Yup.string().required("Job profile type is required")
                    })}
                    onSubmit={async (values) => {
                        try {
                            // await firebaseEmailPasswordSignIn(values.email, values.password);
                            console.log("Signup data: ", values);
                        } catch (err) {
                            console.error(err);
                            if (scriptedRef.current) {
                                setStatus({success: false});
                                setErrors({submit: err.message});
                                setSubmitting(false);
                            }
                        }
                    }}
                >
                    {({errors, handleBlur, handleChange, isSubmitting, touched, values}) => (
                        <Form noValidate {...others}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.companyname && errors.companyname)}
                                        className={classes.loginInput}
                                    >
                                        {/* <InputLabel htmlFor="outlined-adornment-companyname">University</InputLabel> */}

                                        <TextField
                                            id="outlined-textarea"
                                            rows={4}
                                            onChange={(e) => handleFormValueSet(e)}
                                            name={"intro"}
                                            multiline
                                        />

                                        {touched.companyname && errors.companyname && (
                                            <FormHelperText error id="standard-weight-helper-text-companyname">
                                                {" "}
                                                {errors.companyname}{" "}
                                            </FormHelperText>
                                        )}
                                    </FormControl>
                                </Grid>
                                {/* <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.companyname && errors.companyname)}
                                        className={classes.loginInput}
                                    >
                                        <TextField
                                            id="outlined-textarea"
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                        />
                                    </FormControl>
                                </Grid> */}

                                <Grid item xs={12}>
                                    <Box>
                                        <AnimateButton>
                                            <Button
                                                onClick={() => handleSubmit()}
                                                disableElevation
                                                disabled={isSubmitting}
                                                fullWidth
                                                size="large"
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                            >
                                                Submit
                                            </Button>
                                        </AnimateButton>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Form>
                    )}
                </Formik>
            </CardContent>
            <NavigationButtons />
        </Card>
    );
}
