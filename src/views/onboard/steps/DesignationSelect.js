import * as React from "react";
import {designations} from "../../../config";

// material-ui
import {makeStyles} from "@material-ui/styles";
import Snackbar from "@mui/material/Snackbar";

import {Grid, Typography, Card, CardContent, Alert} from "@material-ui/core";

import {gridSpacing} from "store/constant";
import MenuItem from "@mui/material/MenuItem";

// Icons - MUI

// Services.
import {addPreferedDesignation} from "../services";

// Components
import UIMultiselect from "views/ui-elements/basic/UIMultiselect";
import NavigationButtons from "../components/NavigationButtons";
import useNotifications from "../components/useNotifications";

const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px",
        width: "100%"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    buttonNext: {
        textAlign: "right"
    }
}));

export default function DesignationSelect({nextCallback, previousCallback}) {
    const classes = useStyles();
    const {isNotification, message, ShowSnackBar} = useNotifications();
    const [designation, setDesignation] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
    const USER = JSON.parse(localStorage.getItem("user-token"));
    let USER_ID = USER.id;

    const handleChangeDesignation = (e) => {
        try {
            console.log("e", e.target.value.length);
            let lastSelected = e.target.value[e.target.value.length - 1];
            let selectedOption = {
                name: lastSelected,
                skill_id: designation.length
            };
            setDesignation((prevState) => [...prevState, selectedOption]);
            setSelected(e.target.value);
        } catch (error) {
            console.log(error);
        }
    };

    // On click next submit the form.
    const handleSubmit = async () => {
        try {
            // Validation.
            if (designation.length === 0) {
                ShowSnackBar("Please select atleast one designation");
                return;
            } else {
                let tempData = {
                    gender: "M",
                    intro: " ",
                    is_graduate: true,
                    is_intern: false,
                    interest: 5,
                    total_experience: 8,
                    preferred_companies: "Amazon, Google",
                    preferred_designations: JSON.stringify(designation)
                };
                let response = await addPreferedDesignation(USER_ID, tempData);
                // if status = 200 then, call nextstep.
                if (response.status === 200) {
                    ShowSnackBar("Successful");
                    nextCallback();
                    return;
                }
                // else show notification.
                ShowSnackBar("Error in adding designation!");
            }
            // Call the api service.
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <Snackbar open={isNotification} autoHideDuration={6000} onClose={() => ShowSnackBar(null)}>
                <Alert onClose={() => ShowSnackBar(null)} severity="info" sx={{width: "100%"}}>
                    {message}
                </Alert>
            </Snackbar>
            <Card className={classes.root}>
                <CardContent>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} className={classes.aboutContainer}>
                            <Grid item xs={12}>
                                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                                    Perfect! Tell us what designation are you aiming for?{" "}
                                </Typography>
                                <UIMultiselect selected={selected} onChange={handleChangeDesignation}>
                                    {designations.map((name) => (
                                        <MenuItem key={name} value={name}>
                                            {name}
                                        </MenuItem>
                                    ))}{" "}
                                </UIMultiselect>
                            </Grid>
                        </Grid>
                    </Grid>
                    <NavigationButtons nextCallback={handleSubmit} previousCallback={previousCallback} />
                </CardContent>
            </Card>
        </>
    );
}
