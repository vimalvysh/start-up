import * as React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import AnimateButton from "ui-component/extended/AnimateButton";
import Rating from "@material-ui/lab/Rating";
import {
    Box,
    Button,
    Grid,
    Typography,
    FormControl,
    InputLabel,
    Card,
    CardContent,
    Select,
    MenuItem,
    FormHelperText,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Stack
} from "@material-ui/core";

// third party
import {Formik} from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: "100%",
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: "#fff"
    },
    stripStyle: {
        marginBottom: "10px",
        backgroundColor: "#E3F2FD",
        borderRadius: "8px"
    },
    selected: {
        backgroundColor: "#EDE7F6"
    }
}));

export default function ProficientEnglishLanguage_Step12({...others}) {
    const classes = useStyles();
    const [progress, setProgress] = React.useState(20);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const [productRating] = React.useState(4);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    How proficient are you in English Language Skills?
                </Typography>

                <Formik
                    initialValues={{
                        jobProfileType: "",
                        submit: null
                    }}
                    validationSchema={Yup.object().shape({
                        jobProfileType: Yup.string().required("Job profile type is required")
                    })}
                    onSubmit={async (values, {setErrors, setStatus, setSubmitting}) => {
                        try {
                            // await firebaseEmailPasswordSignIn(values.email, values.password);
                            console.log("Signup data: ", values);

                            if (scriptedRef.current) {
                                setStatus({success: true});
                                setSubmitting(false);
                            }
                        } catch (err) {
                            console.error(err);
                            if (scriptedRef.current) {
                                setStatus({success: false});
                                setErrors({submit: err.message});
                                setSubmitting(false);
                            }
                        }
                    }}
                >
                    {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values}) => (
                        <form noValidate onSubmit={handleSubmit} {...others}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <List component="nav" aria-label="main mailbox folders">
                                        <ListItem
                                            className={classes.stripStyle}
                                            button
                                            selected={selectedIndex === 1}
                                            onClick={(event) => handleListItemClick(event, 0)}
                                        >
                                            <Stack direction="row" alignItems="center" spacing={1}>
                                                <Typography variant="strong">Reading</Typography>
                                                <Rating precision={0.5} name="size-small" defaultValue={3.5} />
                                            </Stack>
                                        </ListItem>
                                    </List>
                                </Grid>
                                
                                <Grid item xs={12}>
                                    <List component="nav" aria-label="main mailbox folders">
                                        <ListItem
                                            className={classes.stripStyle}
                                            button
                                            selected={selectedIndex === 1}
                                            onClick={(event) => handleListItemClick(event, 0)}
                                        >
                                            <Stack direction="row" alignItems="center" spacing={1}>
                                                <Typography variant="strong">Writing</Typography>
                                                <Rating precision={0.5} name="size-small" defaultValue={3.5} />
                                            </Stack>
                                        </ListItem>
                                    </List>
                                </Grid>
                                <Grid item xs={12}>
                                    <List component="nav" aria-label="main mailbox folders">
                                        <ListItem
                                            className={classes.stripStyle}
                                            button
                                            selected={selectedIndex === 1}
                                            onClick={(event) => handleListItemClick(event, 0)}
                                        >
                                            <Stack direction="row" alignItems="center" spacing={1}>
                                                <Typography variant="strong">Speaking</Typography>
                                                <Rating precision={0.5} name="size-small" defaultValue={3.5} />
                                            </Stack>
                                        </ListItem>
                                    </List>
                                </Grid>
                                <Grid item xs={12}>
                                    {errors.submit && (
                                        <Box
                                            sx={{
                                                mt: 3
                                            }}
                                        >
                                            <FormHelperText error>{errors.submit}</FormHelperText>
                                        </Box>
                                    )}

                                    <Box>
                                        <AnimateButton>
                                            <Button
                                                disableElevation
                                                disabled={isSubmitting}
                                                fullWidth
                                                size="large"
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                            >
                                                Submit
                                            </Button>
                                        </AnimateButton>
                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    );
}
