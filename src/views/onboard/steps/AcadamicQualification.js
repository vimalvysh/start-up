import * as React from "react";
import {useDispatch} from "react-redux";
import moment from "moment";

// material-ui
import {makeStyles} from "@material-ui/styles";
import AnimateButton from "ui-component/extended/AnimateButton";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import Snackbar from "@mui/material/Snackbar";

import MobileDatePicker from "@mui/lab/MobileDatePicker";
import {
    Box,
    Button,
    Typography,
    Alert,
    Card,
    CardContent,
    Grid,
    TextField,
    FormControl,
    InputLabel,
    FormHelperText,
    OutlinedInput
} from "@material-ui/core";
// icons.
import DownloadForOfflineIcon from "@mui/icons-material/DownloadForOffline";

// Hooks.
import useNotifications from "../components/useNotifications";
// Services.
import {addEducatiion} from "../services";

// third party
import {Formik} from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: "100%",
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: "#fff"
    },
    stripStyle: {
        marginBottom: "10px",
        backgroundColor: "#E3F2FD",
        borderRadius: "8px"
    },
    selected: {
        backgroundColor: "#EDE7F6"
    },
    chipStyle: {
        width: "100%",
        padding: "24px 0",
        borderRadius: "12px"
    }
}));

const initialFormValues = {
    university: "",
    college: "",
    department: "",
    fromDate: new Date(),
    toDate: new Date(),
    currentlyPursuing: true,
    location: ""
};

export default function AcadamicQualification({nextCallback}) {
    const classes = useStyles();

    const USER = JSON.parse(localStorage.getItem("user-token"));
    let USER_ID = USER.id;
    const {isNotification, message, ShowSnackBar} = useNotifications();
    const [isSnackBar, setIsSnackBar] = React.useState(false);

    const [formValues, setFormVaules] = React.useState(initialFormValues);

    const handleSnakBar = () => {
        setIsSnackBar((prevState) => !prevState);
    };

    const handleChangeDate = (newValue, field) => {
        console.log("newValue", moment(newValue).format("DD-MM-YYYY"));
        // setValue(newValue);
        setFormVaules((prevState) => ({
            ...prevState,
            [field]: newValue
        }));
    };

    const handleFormValueSet = (e) => {
        try {
            setFormVaules({
                ...formValues,
                [e.target.name]: e.target.value
            });
        } catch (error) {
            console.log(error);
        }
    };

    const handleSubmit = async (e) => {
        try {
            console.log(e);
            e.preventDefault();
            // Validate the form.
            let isValidated = await handleValidation();
            console.log("isValidated", isValidated);
            if (!isValidated) {
                ShowSnackBar("Please fill all the fields!");
                return;
            }
            let tempData = {
                degree_title: formValues?.department ?? "",
                institute: formValues?.college ?? "",
                location: formValues?.location ?? "",
                start_date: formValues?.fromDate ?? "",
                end_date: formValues?.toDate ?? ""
            };
            let response = await addEducatiion(USER_ID, tempData);
            if (response.status === 200) {
                nextCallback();
            }
        } catch (error) {
            console.log(error);
        }
    };

    // Form validation.
    // Checking each key for values.
    // if no value show notification
    const handleValidation = async () => {
        try {
            // loop through formValues object.
            await Object.keys(formValues).forEach((el) => {
                if (formValues[el].toString().length === 0) {
                    console.log("formValues[el].toString", formValues[el].toString());

                    return false;
                }
            });
            return true;
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Card className={classes.root}>
            <Snackbar open={isNotification} autoHideDuration={6000} onClose={() => ShowSnackBar(null)}>
                <Alert onClose={() => ShowSnackBar(null)} severity="info" sx={{width: "100%"}}>
                    {message}
                </Alert>
            </Snackbar>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    That’s Great! Let us know about your latest academic qualification
                </Typography>

                <Formik
                    initialValues={{
                        jobProfileType: "",
                        submit: null
                    }}
                    validationSchema={Yup.object().shape({
                        jobProfileType: Yup.string().required("Job profile type is required")
                    })}
                    onSubmit={async (values, {setErrors, setStatus, setSubmitting}) => {
                        try {
                            // await firebaseEmailPasswordSignIn(values.email, values.password);
                            console.log("Signup data: ");

                            if (scriptedRef.current) {
                                setStatus({success: true});
                                setSubmitting(false);
                            }
                        } catch (err) {
                            console.error(err);
                            if (scriptedRef.current) {
                                setStatus({success: false});
                                setErrors({submit: err.message});
                                setSubmitting(false);
                            }
                        }
                    }}
                >
                    {({errors, handleBlur, handleChange, isSubmitting, touched, values}) => (
                        <form noValidate>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.companyname && errors.companyname)}
                                        className={classes.loginInput}
                                    >
                                        <InputLabel htmlFor="outlined-adornment-companyname">University</InputLabel>
                                        <OutlinedInput
                                            label="University"
                                            id="outlined-adornment-companyname"
                                            type="text"
                                            value={values.companyname}
                                            name="university"
                                            onBlur={handleBlur}
                                            onChange={handleFormValueSet}
                                            endAdornment={<DownloadForOfflineIcon position="end"></DownloadForOfflineIcon>}
                                            inputProps={{
                                                classes: {
                                                    notchedOutline: classes.notchedOutline
                                                }
                                            }}
                                        />
                                        {touched.companyname && errors.companyname && (
                                            <FormHelperText error id="standard-weight-helper-text-companyname">
                                                {" "}
                                                {errors.companyname}{" "}
                                            </FormHelperText>
                                        )}
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.linkedIn && errors.linkedIn)}
                                        className={classes.loginInput}
                                    >
                                        <InputLabel htmlFor="outlined-adornment-companyname">College</InputLabel>
                                        <OutlinedInput
                                            label="College"
                                            id="outlined-adornment-companyname"
                                            type="text"
                                            value={values.linkedIn}
                                            name="college"
                                            onBlur={handleBlur}
                                            onChange={handleFormValueSet}
                                            endAdornment={<DownloadForOfflineIcon position="end"></DownloadForOfflineIcon>}
                                            inputProps={{
                                                classes: {
                                                    notchedOutline: classes.notchedOutline
                                                }
                                            }}
                                        />
                                        {touched.linkedIn && errors.linkedIn && (
                                            <FormHelperText error id="standard-weight-helper-text-companyname">
                                                {" "}
                                                {errors.linkedIn}{" "}
                                            </FormHelperText>
                                        )}
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.linkedIn && errors.linkedIn)}
                                        className={classes.loginInput}
                                    >
                                        <InputLabel htmlFor="outlined-adornment-companyname">Department</InputLabel>
                                        <OutlinedInput
                                            label="Department"
                                            id="outlined-adornment-companyname"
                                            type="text"
                                            value={values.linkedIn}
                                            name="department"
                                            onBlur={handleBlur}
                                            onChange={handleFormValueSet}
                                            endAdornment={<DownloadForOfflineIcon position="end"></DownloadForOfflineIcon>}
                                            inputProps={{
                                                classes: {
                                                    notchedOutline: classes.notchedOutline
                                                }
                                            }}
                                        />
                                        {touched.linkedIn && errors.linkedIn && (
                                            <FormHelperText error id="standard-weight-helper-text-companyname">
                                                {" "}
                                                {errors.linkedIn}{" "}
                                            </FormHelperText>
                                        )}
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={6}>
                                            <FormControl
                                                fullWidth
                                                error={Boolean(touched.linkedIn && errors.linkedIn)}
                                                className={classes.loginInput}
                                            >
                                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                                    <MobileDatePicker
                                                        label="From"
                                                        inputFormat="MM/dd/yyyy"
                                                        value={formValues.from}
                                                        name={"fromDate"}
                                                        onChange={(e) => handleChangeDate(e, "fromDate")}
                                                        renderInput={(params) => <TextField {...params} />}
                                                    />
                                                </LocalizationProvider>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <FormControl
                                                fullWidth
                                                error={Boolean(touched.linkedIn && errors.linkedIn)}
                                                className={classes.loginInput}
                                            >
                                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                                    <MobileDatePicker
                                                        label="To"
                                                        inputFormat="MM/dd/yyyy"
                                                        value={formValues.to}
                                                        name={"toDate"}
                                                        onChange={(e) => handleChangeDate(e, "toDate")}
                                                        renderInput={(params) => <TextField {...params} />}
                                                    />
                                                </LocalizationProvider>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.linkedIn && errors.linkedIn)}
                                        className={classes.loginInput}
                                    >
                                        <InputLabel htmlFor="outlined-adornment-companyname">Currently Pursuing</InputLabel>
                                        <OutlinedInput
                                            label="Currently Pursuing"
                                            id="outlined-adornment-companyname"
                                            type="text"
                                            value={values.linkedIn}
                                            name="currentlyPursuing"
                                            onBlur={handleBlur}
                                            onChange={handleFormValueSet}
                                            endAdornment={<DownloadForOfflineIcon position="end"></DownloadForOfflineIcon>}
                                            inputProps={{
                                                classes: {
                                                    notchedOutline: classes.notchedOutline
                                                }
                                            }}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl
                                        fullWidth
                                        error={Boolean(touched.linkedIn && errors.linkedIn)}
                                        className={classes.loginInput}
                                    >
                                        <InputLabel htmlFor="outlined-adornment-companyname">Lcoation</InputLabel>
                                        <OutlinedInput
                                            id="outlined-adornment-companyname"
                                            type="text"
                                            value={values.linkedIn}
                                            name="location"
                                            onBlur={handleBlur}
                                            onChange={handleFormValueSet}
                                            endAdornment={<DownloadForOfflineIcon position="end"></DownloadForOfflineIcon>}
                                            inputProps={{
                                                classes: {
                                                    notchedOutline: classes.notchedOutline
                                                }
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12}>
                                    <Box>
                                        <AnimateButton>
                                            <Button
                                                onClick={(e) => handleSubmit(e)}
                                                disableElevation
                                                disabled={isSubmitting}
                                                fullWidth
                                                size="large"
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                            >
                                                Submit
                                            </Button>
                                        </AnimateButton>
                                    </Box>
                                </Grid>
                            </Grid>
                        </form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    );
}
