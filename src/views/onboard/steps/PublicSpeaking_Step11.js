import * as React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import AnimateButton from "ui-component/extended/AnimateButton";
import {
    Box,
    Button,
    Grid,
    Paper,
    Typography,
    FormControl,
    InputLabel,   
    Card,
    CardContent,    
    Select,
    MenuItem,
    FormHelperText,
   FormControlLabel, Radio, RadioGroup
} from "@material-ui/core";

// third party
import {Formik} from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: '100%',
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: '#fff'
    },
    stripStyle : {
        marginBottom: '10px',
        backgroundColor: '#E3F2FD',
        borderRadius: '8px',
    },
    selected : {
        backgroundColor: '#EDE7F6',
    }
}));

export default function PublicSpeaking_Step11({...others}) {
    const classes = useStyles();
    const [progress, setProgress] = React.useState(20);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const [valueLabel, setValueLabel] = React.useState('Yes');

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                    Are you confident in Public Speaking Skills?
                </Typography>

                <Formik
                    initialValues={{
                        jobProfileType: "",
                        submit: null
                    }}
                    validationSchema={Yup.object().shape({
                        jobProfileType: Yup.string().required("Job profile type is required")
                    })}
                    onSubmit={async (values, {setErrors, setStatus, setSubmitting}) => {
                        try {
                            // await firebaseEmailPasswordSignIn(values.email, values.password);
                            console.log("Signup data: ", values);

                            if (scriptedRef.current) {
                                setStatus({success: true});
                                setSubmitting(false);
                            }
                        } catch (err) {
                            console.error(err);
                            if (scriptedRef.current) {
                                setStatus({success: false});
                                setErrors({submit: err.message});
                                setSubmitting(false);
                            }
                        }
                    }}
                >
                    {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values}) => (
                        <form noValidate onSubmit={handleSubmit} {...others}>
                            <Grid container spacing={3}>
                          
                            <Grid item xs={12}>
                            <Paper variant="outlined" xs={12} sx={{pl:2}}>
                                <FormControl>
                                    <RadioGroup
                                        row
                                        aria-label="gender"
                                        value={valueLabel}
                                        onChange={(e) => setValueLabel(e.target.value)}
                                        name="row-radio-buttons-group"
                                    >
                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                    </RadioGroup>
                                </FormControl>
                                </Paper>
                            </Grid>
                       
                            </Grid>
                        </form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    );
}
