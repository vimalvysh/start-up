import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {
    Checkbox,
    FormControlLabel,
    Grid,
    Button,
    Typography,
    TextField,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Card,
    CardContent,
    Chip
} from "@material-ui/core";
import AnimateButton from "ui-component/extended/AnimateButton";

// Icons - MUI

import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import WorkIcon from "@material-ui/icons/Work";
import BookIcon from "@material-ui/icons/Book";
import PersonIcon from "@material-ui/icons/Person";
import FlightTakeoffIcon from "@material-ui/icons/FlightTakeoff";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import SchoolIcon from "@material-ui/icons/School";
import {onboardUpdateUserCreator} from "store/actions/onboardUserActions";
import LoadingBox from "../../pages/components/LoadingBox";
import MessageBox from "../../pages/components/MessageBox";

// components.
import NavigationButtons from "../components/NavigationButtons";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: "100%",
        backgroundColor: theme.palette.background.paper
    },
    bgWhite: {
        backgroundColor: "#fff"
    },
    stripStyle: {
        marginBottom: "10px",
        backgroundColor: "#F2F7F9",
        borderRadius: "8px"
    },
    muiSelected: {
        marginBottom: "10px",
        borderRadius: "8px",
        backgroundColor: "#EDE7F6 !important",
        color: "#5E35B1 !important"
    }
}));

export default function LookingFor({nextCallback}) {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [progress, setProgress] = React.useState(10);
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(false);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    let onboardUpdateReq = {};
    let [chosenValue, setChosenValue] = React.useState(1);
    const userToken = JSON.parse(localStorage.getItem("user-token"));
    // const onboardUpdateUserInfo = useSelector((state) => state.onboardUpdateUserInfo);
    // const { loading, error, onboardUpdateInfo} = onboardUpdateUserInfo;
    const handleListItemClick = (event) => {
        onboardUpdateReq = {
            intentionId: chosenValue,
            lastStep: userToken.microserviceFields.LAST_STEP + 1,
            status: userToken.microserviceFields.STATUS,
            userId: userToken.microserviceFields.USER_ID
        };
        dispatch(onboardUpdateUserCreator(onboardUpdateReq));
        nextCallback(event);
    };

    const setState = (event, index) => {
        setSelectedIndex(index + 1);
        setChosenValue(index + 1);
    };

    return (
        <div>
            {loading ? (
                <LoadingBox></LoadingBox>
            ) : error ? (
                <MessageBox variant="danger">{error}</MessageBox>
            ) : (
                <Card className={classes.root}>
                    <CardContent>
                        <Typography variant="h5" gutterBottom sx={{mb: 2}}>
                            That’s great! What are you looking for?
                        </Typography>

                        <div className={classes.root}>
                            <List component="nav" aria-label="main mailbox folders">
                                <ListItem
                                    className={selectedIndex === 1 ? classes.muiSelected : classes.stripStyle}
                                    button
                                    selected={selectedIndex === 0}
                                    onClick={(event) => {
                                        setState(event, 0);
                                    }}
                                >
                                    <ListItemIcon>
                                        <WorkIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Growth in any field will be fine" />
                                    <Chip label="01" color="primary" size="small" />
                                </ListItem>

                                <ListItem
                                    className={selectedIndex === 2 ? classes.muiSelected : classes.stripStyle}
                                    button
                                    selected={selectedIndex === 1}
                                    onClick={(event) => {
                                        setState(event, 1);
                                    }}
                                >
                                    <ListItemIcon>
                                        <BookIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="I would prefer growth in a specific field" />
                                    <Chip label="02" color="primary" size="small" />
                                </ListItem>
                                <ListItem
                                    className={selectedIndex === 3 ? classes.muiSelected : classes.stripStyle}
                                    button
                                    selected={selectedIndex === 2}
                                    onClick={(event) => {
                                        setState(event, 2);
                                    }}
                                >
                                    <ListItemIcon>
                                        <SchoolIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Provide Mentorship" />
                                    <Chip label="03" color="primary" size="small" />
                                </ListItem>
                            </List>
                            {/* <AnimateButton>
                                <Button variant="contained" onClick={(event) => { handleListItemClick(event) }} >
                                    Next
                                </Button>
                            </AnimateButton> */}
                        </div>
                        <NavigationButtons nextCallback={nextCallback} />
                    </CardContent>
                </Card>
            )}
        </div>
    );
}
