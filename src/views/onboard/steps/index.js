import React from "react";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/styles";
// material-ui
import {Button, Grid, Paper, Step, Stepper, StepLabel, Stack, Typography, Card, CardContent} from "@material-ui/core";
import SubCard from "ui-component/cards/SubCard";
// project imports
import AddressForm from "./AddressForm";
import PaymentForm from "./PaymentForm";

// material-ui
import {withStyles, useTheme} from "@material-ui/styles";
import {Box, CircularProgress, LinearProgress} from "@material-ui/core";

// Onboard - steps
import CareerStory_Step1 from "./CareerStory_Step1";
import LookingFor from "./LookingFor";
import CareerSelect from "./CareerSelect";

import Review from "./Review";
import MainCardOnboard from "ui-component/cards/MainCardOnboard";
import AnimateButton from "ui-component/extended/AnimateButton";
import SecondaryAction from "ui-component/cards/CardSecondaryAction";
import AcadamicQualification from "./AcadamicQualification";
import LandYourCareer_Step5 from "./LandYourCareer_Step5";
import DesignationSelect from "./DesignationSelect";
import AimingDesignation_Step7 from "./AimingDesignation_Step7";
import KeySkillsSelect from "./KeySkillsSelect";
import RatePreparedness from "./RatePreparedness";
import ResumeUpload from "./ResumeUpload";

// step options
const steps = [
    //"Career Story",
    "Looking For",
    "Experience Years",
    "Last Company Experience",
    //"Land Your Career",
    "Latest Academic Qualification",
    //"Aiming Designation",
    "Key Skill And Rating",
    "Rate Your Preparedness",
    "Rate yourself on Interview Selection skills?",
    "Are you confident in Public Speaking Skills?",
    "How proficient are you in English Language Skills?"
];
let childAction;
function getStepContent(step, handleNext, handlePrevious) {
    switch (step) {
        case 1:
            return <LookingFor nextCallback={handleNext} />;
        case 2:
            return <CareerSelect nextCallback={handleNext} previousCallback={handlePrevious} />;
        case 3:
            return <AcadamicQualification nextCallback={handleNext} previousCallback={handlePrevious} />;

        case 4:
            return <DesignationSelect nextCallback={handleNext} previousCallback={handlePrevious} />;

        case 5:
            return <KeySkillsSelect nextCallback={handleNext} previousCallback={handlePrevious} />;
        case 6:
            return <RatePreparedness nextCallback={handleNext} previousCallback={handlePrevious} />;
        case 7:
            return <ResumeUpload nextCallback={handleNext} previousCallback={handlePrevious} />;

        default:
            throw new Error("Unknown step");
    }
}

function LinearProgressWithLabel({value, ...others}) {
    return (
        <Box
            sx={{
                display: "flex",
                alignItems: "center"
            }}
        >
            <Box
                sx={{
                    width: "100%",
                    mr: 1
                }}
            >
                <LinearProgress value={value} variant="determinate" {...others} />
            </Box>
            <Box
                sx={{
                    minWidth: 35
                }}
            >
                <Typography variant="body2" color="textSecondary">{`${Math.round(value)}%`}</Typography>
            </Box>
        </Box>
    );
}

LinearProgressWithLabel.propTypes = {
    value: PropTypes.number
};

// style constant
const BorderLinearProgress = withStyles({
    root: {
        height: 10,
        borderRadius: 5
    },
    bar: {
        borderRadius: 5
    }
})(LinearProgress);

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        backgroundColor: "#E3F2FD"
    },
    bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    }
});
// ===========================|| FORMS WIZARD - BASIC ||=========================== //

const BasicWizard = () => {
    const [activeStep, setActiveStep] = React.useState(1);
    const classes = useStyles();
    const [progress, setProgress] = React.useState(14.2);
    const [buffer, setBuffer] = React.useState(10);

    const handleNext = () => {
        setActiveStep(activeStep + 1);
        setProgress(progress + 14.2);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
        setProgress(progress - 14.2);
    };

    return (
        <MainCardOnboard>
            <Card className={classes.root}>
                <Grid item xs={12} sm={12} sx={{mb: 2}}>
                    <SubCard>
                        <Grid container direction="column" spacing={3}>
                            <Grid item xs={12} md={12}>
                                <Grid container spacing={2} alignItems="center" justifyContent="center">
                                    <Grid container spacing={3} item>
                                        <Grid item xs={6}>
                                            <Paper className={classes.paper}>
                                                <Typography variant="caption">Progress</Typography>
                                            </Paper>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Paper className={classes.paper}>
                                                <Typography variant="h6" align="right">
                                                    {Math.round(progress)}%
                                                </Typography>
                                            </Paper>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs>
                                        <BorderLinearProgress variant="determinate" color="secondary" value={progress} />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </SubCard>
                </Grid>
                {/* <Stepper activeStep={activeStep} sx={{pt: 3, pb: 3}}>
                    {steps.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper> */}
            </Card>

            <>
                {activeStep === steps.length ? (
                    <>
                        {/* <Typography variant="h5" gutterBottom>
                            Thank you for your order.
                        </Typography>
                        <Typography variant="subtitle1">
                            Your order number is #2001539. We have emailed your order confirmation, and will send you an update when your
                            order has shipped.
                        </Typography>
                        <Stack direction="row" justifyContent="flex-end">
                            <AnimateButton>
                                <Button variant="contained" color="error" onClick={() => setActiveStep(0)} sx={{ my: 3, ml: 1 }}>
                                    Reset
                                </Button>
                            </AnimateButton>
                        </Stack> */}
                    </>
                ) : (
                    <>{getStepContent(activeStep, handleNext, handleBack)}</>
                )}
            </>
        </MainCardOnboard>
    );
};

export default BasicWizard;
