import * as React from "react";

export default function useNotifications() {
    const [isNotification, setIsNotification] = React.useState(false);
    const [message, setMessage] = React.useState("");

    const ShowSnackBar = (mesg = null) => {
        console.log("in snck", mesg);
        if (mesg) {
            setIsNotification(true);
            setMessage(mesg);
        } else {
            setIsNotification(false);
            setMessage(null);
        }
    };
    return {isNotification, message, ShowSnackBar};
}
