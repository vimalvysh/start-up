import * as React from "react";

import {Stack, Button} from "@material-ui/core";

export default function NavigationBottons({nextCallback, previousCallback}) {
    return (
        <Stack direction="row" justifyContent={previousCallback ? "space-between" : "flex-end"}>
            {previousCallback && (
                <Button onClick={previousCallback} sx={{my: 3, ml: 1}}>
                    Back
                </Button>
            )}
            {nextCallback && (
                <Button onClick={nextCallback} sx={{my: 3, ml: 1}}>
                    Next
                </Button>
            )}
        </Stack>
    );
}
