import fetchInterceptor from "../../../utils/fetchInterceptor";

//#1: generateToken
export const addEducatiion = async (userId, incomingData) => {
    try {
        const {data, status} = await fetchInterceptor.post(`/cv/api/educations/${userId}`, incomingData);
        console.log("data", status);
        return {
            data,
            status
        };
    } catch (error) {
        console.log(error);

        return {
            data: error,
            status: error.status
        };
    }
};

// add sklill

export const addSkill = async (userId, incomingData) => {
    try {
        const {data, status} = await fetchInterceptor.post(`/cv/api/skills/${userId}`, incomingData);
        console.log("data", data);
        return {
            data,
            status
        };
    } catch (error) {
        console.log(error);

        return {
            data: error,
            status: error.status
        };
    }
};

//  add preffered designation

export const addPreferedDesignation = async (userId, incomingData) => {
    try {
        const {data, status} = await fetchInterceptor.post(`/cv/api/profile/update/${userId}`, incomingData);
        console.log("data", data);
        return {
            data,
            status
        };
    } catch (error) {
        console.log(error);

        return {
            data: error,
            status: error.status
        };
    }
};

//  add preffered designation

export const updateProfile = async (userId, incomingData) => {
    try {
        const {data, status} = await fetchInterceptor.post(`/cv/api/profile/update/${userId}`, incomingData);
        console.log("data", data);
        return {
            data,
            status
        };
    } catch (error) {
        console.log(error);

        return {
            data: error,
            status: error.status
        };
    }
};
