import React, {useEffect, useState} from "react";
import {useDispatch} from "react-redux";

// material-ui
import {useTheme, makeStyles, withStyles} from "@material-ui/styles";
import {Grid, Typography, useMediaQuery, LinearProgress} from "@material-ui/core";
import UserCard from "../../application/users/list/Style2/UserList";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";

//Store
// import stateConfig from 'store/stateConfig';
import {validateUserCreator} from "store/actions/authActions";

import TotalIncomeDarkCard from "./TotalIncomeDarkCard";
import TotalIncomeLightCard from "./TotalIncomeLightCard";

// project imports

import MainCard from "ui-component/cards/MainCard";
import RevenueCard from "ui-component/cards/RevenueCard";
import UserCountCard from "ui-component/cards/UserCountCard";
import {gridSpacing} from "store/constant";

// assets
import {IconShare, IconAccessPoint, IconCircles, IconCreditCard} from "@tabler/icons";
import MonetizationOnTwoToneIcon from "@material-ui/icons/MonetizationOnTwoTone";
import AccountCircleTwoTone from "@material-ui/icons/AccountCircleTwoTone";
import DescriptionTwoToneIcon from "@material-ui/icons/DescriptionTwoTone";
import MenuCard from "layout/MainLayout/Sidebar/MenuCard";
import RecentConversations from "views/application/users/list/Style2/RecentConversations";
import UpcomingAspirantsMeetings from "views/application/contact/UpcomingAspirantsMeetings";
import UpcomingEvents from "views/application/calendar/UpcomingEvents";
import SetSkillsCard from "./SetSkillsCard";
import UploadResumeCard from "./UploadResumeCard";
import AddEducationCard from "./AddEducationCard";
import AddExperienceCard from "./AddExperienceCard";

// style constant
const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 275,
        marginBottom: "22px"
    },
    flatCardBody: {
        "& >div": {
            padding: "0px !important"
        },
        "& svg": {
            width: "50px",
            height: "50px",
            color: theme.palette.secondary.main,
            borderRadius: "14px",
            padding: "10px",
            backgroundColor: theme.palette.mode === "dark" ? theme.palette.background.default : theme.palette.primary.light
        }
    },
    flatCardBlock: {
        padding: "20px",
        borderLeft: "1px solid ",
        borderBottom: "1px solid ",
        borderLeftColor: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[200],
        borderBottomColor: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[200]
    }
}));

// progress bar style
const BorderLinearProgress = withStyles((theme) => ({
    root: {
        height: 10,
        borderRadius: 30
    },
    colorPrimary: {
        backgroundColor: theme.palette.mode === "dark" ? theme.palette.dark.light : "#fff"
    },
    bar: {
        borderRadius: 30,
        backgroundColor: theme.palette.mode === "dark" ? theme.palette.primary.dark : theme.palette.primary.main
    }
}))(LinearProgress);
// ===========================|| ANALYTICS DASHBOARD ||=========================== //

const Dashboard = ({value, ...others}) => {
    const [isLoading, setLoading] = useState(true);
    const theme = useTheme();
    const classes = useStyles();
    const dispatch = useDispatch();

    const matchDownXs = useMediaQuery(theme.breakpoints.down("xs"));

    setTimeout(() => {
        dispatch(validateUserCreator());
    }, 500);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} lg={8.5} md={6}>
                <Card className={classes.root} variant="outlined">
                    <CardContent>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <Grid container alignItems="center" spacing={gridSpacing}>
                                    <Grid item>
                                        <Typography variant="caption">Complete Profile</Typography>
                                        <Typography variant="h6" component="div">
                                            {"80%"}
                                        </Typography>
                                    </Grid>
                                    <Grid item sm zeroMinWidth>
                                        <LinearProgress variant="determinate" value={80} color="primary" {...others} />
                                        {/* <BorderLinearProgress value={78} variant="determinate" {...others} /> */}
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} lg={3}>
                                <AddExperienceCard />
                            </Grid>
                            <Grid item xs={12} lg={3}>
                                <AddEducationCard />
                            </Grid>
                            <Grid item xs={12} lg={3}>
                                <SetSkillsCard />
                            </Grid>
                            <Grid item xs={12} lg={3}>
                                <UploadResumeCard />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
                <Grid item xs={12} lg={12} md={12}>
                    <Card className={classes.root} variant="outlined">
                        <CardContent>
                            <UserCard />
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} lg={12} md={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} lg={5}>
                            <UpcomingAspirantsMeetings />
                        </Grid>
                        <Grid item xs={12} lg={7}>
                            <UpcomingEvents />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            <Grid item xs={12} lg={3.5} md={6}>
                <Card className={classes.root} variant="outlined">
                    <CardContent>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <UserCountCard
                                    primary="UPDATE CALENDAR"
                                    secondary="Share your availablity"
                                    iconPrimary={AccountCircleTwoTone}
                                    color={theme.palette.secondary.main}
                                />
                            </Grid>
                            {/* <Grid item xs={12}>
                                <MenuCard />
                            </Grid> */}
                        </Grid>
                    </CardContent>
                </Card>

                <Card className={classes.root} variant="outlined">
                    <CardContent>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <RecentConversations />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
};

export default Dashboard;
