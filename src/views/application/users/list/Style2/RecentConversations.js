import React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {AvatarGroup, Button, Grid, Chip, LinearProgress, Table, TableBody, TableCell, TableRow,  InputAdornment, OutlinedInput, Typography} from "@material-ui/core";

// assets
import { IconSearch } from '@tabler/icons';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
// project imports
import Avatar from "ui-component/extended/Avatar";
import {gridSpacing} from "store/constant";

// asset
import Avatar1 from "assets/images/users/avatar-1.png";
import Avatar2 from "assets/images/users/avatar-2.png";
import Avatar3 from "assets/images/users/avatar-3.png";
import Avatar4 from "assets/images/users/avatar-4.png";
import Avatar5 from "assets/images/users/avatar-5.png";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ChatBubbleTwoToneIcon from "@material-ui/icons/ChatBubbleTwoTone";
import BlockTwoToneIcon from "@material-ui/icons/BlockTwoTone";
import { fontSize } from "@material-ui/system";

const useStyles = makeStyles((theme) => ({
    successBadge: {
        color: theme.palette.success.dark,
        width: "14px",
        height: "14px"
    },
    searchRecent: {
        width: '60%'
    },
    tableAvatar: {
        width: "35px",
        height: "35px"
    },
    btnTable: {
        borderRadius: "4px",
        paddingLeft: "4px",
        paddingRight: "4px",
        width: "100%",
        minWidth: "120px",
        "&:hover": {
            background: theme.palette.secondary.main,
            borderColor: theme.palette.secondary.main,
            color: "#fff"
        }
    },
    tableResponsive: {
        overflowX: "auto"
    },
    profileTable: {
        "& td": {
            whiteSpace: "nowrap"
        },
        "& td:first-child": {
            paddingLeft: "0px"
        },
        "& td:last-child": {
            paddingRight: "0px",
            minWidth: "260px"
        },
        "& tbody tr:last-child  td": {
            borderBottom: "none"
        },
        [theme.breakpoints.down("lg")]: {
            "& tr:not(:last-child)": {
                borderBottom: "1px solid",
                borderBottomColor: theme.palette.mode === "dark" ? "rgb(132, 146, 196, .2)" : "rgba(224, 224, 224, 1)"
            },
            "& td": {
                display: "inline-block",
                borderBottom: "none",
                paddingLeft: "0px"
            },
            "& td:first-child": {
                display: "block"
            }
        }
    },
    tableSubContent: {      
        fontSize: '11px'
    },
    tableTime: {      
        fontSize: '10px'       
    },
    tableName: {      
        fontSize: '14px'
    },
    searchRecent: {
        padding: '0',
        paddingLeft: '6px'      
    }
}));

// table data
function createData(image, name, subContent, time, location) {
    return {image, name, subContent, time, location};
}

const rows = [
    createData(
        Avatar1,
        "Kattie Abram",       
        "Last message line comes",        
        "8:30 PM"           
    ),
    createData(
        Avatar2,
        "Maudie",      
        "Necessitatibus dolor aut",       
        "9:30 PM"    
    ),
    createData(
        Avatar3,
        "Parto",       
        "Last message line comes",        
        "10:30 PM" 
    ),
    createData(
        Avatar4,
        "James",      
        "Necessitatibus dolor aut",       
        "7:30 PM"
    )
];

// ===========================|| USER LIST 2 ||=========================== //

const RecentConversations = () => {
    const classes = useStyles();
    return (
        <div className={classes.tableResponsive}>
             <Grid item sm={12}>
                <Typography variant="h5" align="left" className={classes.spanStyle}>
                    {'Recent Conversations'}
                </Typography>               
            </Grid>
            <Grid item xs zeroMinWidth mt={2}>
                <OutlinedInput
                    className={classes.searchRecent}
                    id="input-search-card-style1"
                    placeholder="Search"
                    fullWidth
                    startAdornment={
                        <InputAdornment position="start">
                            <IconSearch stroke={1.5} size="1rem" />
                        </InputAdornment>
                    }
                />
            </Grid>
            <Table className={classes.profileTable}>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index}>
                            <TableCell>
                                <Grid container spacing={1}>
                                    <Grid item>
                                        <Avatar alt="User 1" src={row.image} className={classes.tableAvatar} />
                                    </Grid>
                                    <Grid item sm zeroMinWidth>
                                        <Grid container spacing={0}>
                                            <Grid item xs={12}>
                                                <Typography align="left" variant="subtitle1" className={classes.tableName}>
                                                    {row.name}{" "}
                                                    <Chip
                                                        label={row.time}                                                       
                                                        color="primary"
                                                        className={classes.tableTime}
                                                        size="small"
                                                    />
                                                </Typography>                                               
                                                <Typography align="left" variant="subtitle2" className={classes.tableSubContent}>
                                                    {row.subContent}
                                                </Typography>
                                            </Grid>                                           
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </TableCell>                         
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </div>
    );
};

export default RecentConversations;
