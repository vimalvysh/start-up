import React, { useEffect, useState} from "react";
import { useDispatch ,useSelector } from "react-redux";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {AvatarGroup, Button, Grid, LinearProgress, Table, TableBody, TableCell, TableRow, Typography} from "@material-ui/core";

// assets
import LayersTwoToneIcon from '@material-ui/icons/LayersTwoTone';

// assets
import { IconChecks, IconSettings } from '@tabler/icons';
// project imports
import Avatar from "ui-component/extended/Avatar";
import {gridSpacing} from "store/constant";

// asset
import Avatar1 from "assets/images/users/avatar-1.png";
import Avatar2 from "assets/images/users/avatar-2.png";
import Avatar3 from "assets/images/users/avatar-3.png";
import Avatar4 from "assets/images/users/avatar-4.png";
import Avatar5 from "assets/images/users/avatar-5.png";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import PermContactCalendarSharpIcon from '@material-ui/icons/PermContactCalendarSharp';
import DoneAllIcon from "@material-ui/icons/DoneAll";
import ChatBubbleTwoToneIcon from "@material-ui/icons/ChatBubbleTwoTone";
import BlockTwoToneIcon from "@material-ui/icons/BlockTwoTone";

const useStyles = makeStyles((theme) => ({
    successBadge: {
        color: theme.palette.success.dark,
        width: "14px",
        height: "14px"
    },
    tableAvatar: {
        width: "60px",
        height: "60px"
    },
    btnTable: {
        borderRadius: "4px",
        paddingLeft: "4px",
        paddingRight: "4px",
        width: "100%",
        minWidth: "120px",
        "&:hover": {
            background: theme.palette.secondary.main,
            borderColor: theme.palette.secondary.main,
            color: "#fff"
        }
    },
    tableResponsive: {
        overflowX: "auto"
    },
    profileTable: {
        "& td": {
            whiteSpace: "nowrap"
        },
        "& td:first-child": {
            paddingLeft: "0px"
        },
        "& td:last-child": {
            paddingRight: "0px",
            minWidth: "260px"
        },
        "& tbody tr:last-child  td": {
            borderBottom: "none"
        },
        [theme.breakpoints.down("lg")]: {
            "& tr:not(:last-child)": {
                borderBottom: "1px solid",
                borderBottomColor: theme.palette.mode === "dark" ? "rgb(132, 146, 196, .2)" : "rgba(224, 224, 224, 1)"
            },
            "& td": {
                display: "inline-block",
                borderBottom: "none",
                paddingLeft: "0px"
            },
            "& td:first-child": {
                display: "block"
            }
        }
    },
    tableSubContent: {
        whiteSpace: "break-spaces"
    }
}));

// table data
function createData(image, name, designation, badgeStatus, subContent, email, phone, time, location) {
    return {image, name, designation, badgeStatus, subContent, email, phone, time, location};
}

const rows = [
    createData(
        Avatar1,
        "Kattie Abram",
        "University / Office Name",
        "active",
        "We need to generate the virtual CSS hard drive!",
        "Kattie40@gmail.com",
        "506-654-1653",
        "24 Oct 8:30 PM",
        "Saucerize"
    ),
    createData(
        Avatar2,
        "Maudie",
        "Product Solutions Administrator",
        "active",
        "If we synthesize the protocol, we can get to the RSS circuit through.",
        "Maudie@hotmail.com",
        "673-157-1670",
        "21 Oct 9:30 PM",
        "Port Narcos"
    )
];

// ===========================|| USER LIST 2 ||=========================== //

const UserCard = (props) => {
    const classes = useStyles(); 
    const validateUserInfo = useSelector((state) => state.validateUserInfo);
    console.log('#mentorData: ', props);
       
    return (
        <div className={classes.tableResponsive}>
            <Grid item sm={12} mb={2}>
                <Typography variant="h5" align="left" className={classes.spanStyle}>
                    {"Meeting Requests"}
                </Typography>
            </Grid>
            <Table className={classes.profileTable}>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index}>
                            <TableCell>
                                <Grid container spacing={2}>
                                    <Grid item>
                                        <Avatar alt="User 1" src={row.image} className={classes.tableAvatar} />
                                    </Grid>
                                    <Grid item sm zeroMinWidth>
                                        <Grid container spacing={1}>
                                            <Grid item xs={12}>
                                                <Typography align="left" variant="subtitle1">
                                                    {row.name}{" "}
                                                    {row.badgeStatus === "active" && <CheckCircleIcon className={classes.successBadge} />}
                                                </Typography>
                                                <Typography align="left" variant="subtitle2" className={classes.tableSubContent}>
                                                    {row.designation}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={2} mt={0.2}>
                                            <Grid item xs={12}>
                                                <Typography variant="caption">Email</Typography>
                                                <Typography variant="h6">{row.email}</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </TableCell>
                            <TableCell>
                                <Grid container spacing={2}>                                   
                                    <Grid item xs={12}>                                        
                                        <Typography variant="caption">Time</Typography>
                                        <Typography variant="h6">{row.time}</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography variant="caption">Phone</Typography>
                                        <Typography variant="h6">{row.phone}</Typography>
                                    </Grid>
                                </Grid>
                            </TableCell>

                            <TableCell>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} align="center">
                                        <Button className={classes.btnStyle} variant="contained" startIcon={<PermContactCalendarSharpIcon />}>
                                            Check Profile
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} container spacing={1}>
                                        <Grid item xs={6}>
                                            <Button fullWidth variant="outlined" size="small" startIcon={<DoneAllIcon />}>
                                                Approve
                                            </Button>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Button
                                                fullWidth
                                                variant="outlined"
                                                color="error"
                                                size="small"
                                                startIcon={<BlockTwoToneIcon />}
                                            >
                                                Reject
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </div>
    );
};

export default UserCard;
