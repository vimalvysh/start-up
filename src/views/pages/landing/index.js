import React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Divider, Grid, Stack, Typography, useMediaQuery, Button, Box, Paper} from "@material-ui/core";
import Logo from "ui-component/Logo";

import Register from "../authentication/authentication3/Register";
import DashboardDefault from "../../dashboard/Default";

// style constant
const useStyles = makeStyles((theme) => ({
    header: {
        paddingTop: "30px",
        overflowX: "hidden",
        overflowY: "clip",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "0px"
        }
    },
    sectionWhite: {
        paddingTop: "160px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "60px"
        }
    }
}));

// =============================|| LANDING MAIN ||============================= //

const Landing = () => {
    const classes = useStyles();

    return (
        <>
            <div id="home">
                {/* className={classes.header} */}
                <Register />
            </div>
        </>
    );
};

export default Landing;
