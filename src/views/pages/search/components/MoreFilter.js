import React from "react";

// material-ui
import {makeStyles, useTheme} from "@material-ui/styles";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import FormLabel from "@mui/material/FormLabel";
import Checkbox from "@mui/material/Checkbox";

import MainCard from "ui-component/cards/MainCard";
import {Grid} from "@material-ui/core";

// Icons.
import CloseIcon from "@mui/icons-material/Close";

// style constant
const useStyles = makeStyles((theme) => ({
    filterContainer: {
        zIndex: 1,
        position: "absolute",
        top: "180px",
        right: "100px"
    }
}));
const SamplePage = ({isShowFiletrs, handleToggleMoreFilter}) => {
    const classes = useStyles();
    const [state, setState] = React.useState({
        gilad: true,
        jason: false,
        antoine: false
    });

    const handleChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.checked
        });
    };

    const {gilad, jason, antoine} = state;
    const error = [gilad, jason, antoine].filter((v) => v).length !== 1;

    return (
        isShowFiletrs && (
            <MainCard
                title={
                    <Grid container justifyContent={"space-between"}>
                        <Grid>More Filters</Grid>
                        <Grid onClick={handleToggleMoreFilter}>
                            <CloseIcon />
                        </Grid>
                    </Grid>
                }
                className={classes.filterContainer}
            >
                <Box sx={{display: "flex"}}>
                    <FormControl s component="fieldset" variant="standard">
                        <FormLabel component="legend">Location</FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={gilad} onChange={handleChange} name="gilad" />}
                                label="Gilad Gray"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={jason} onChange={handleChange} name="jason" />}
                                label="Jason Killian"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={antoine} onChange={handleChange} name="antoine" />}
                                label="Antoine Llorca"
                            />
                        </FormGroup>
                        <FormHelperText>Be careful</FormHelperText>
                    </FormControl>
                    <FormControl required error={error} component="fieldset" sx={{mt: 3}} variant="standard">
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={gilad} onChange={handleChange} name="gilad" />}
                                label="Gilad Gray"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={jason} onChange={handleChange} name="jason" />}
                                label="Jason Killian"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={antoine} onChange={handleChange} name="antoine" />}
                                label="Antoine Llorca"
                            />
                        </FormGroup>
                        <FormHelperText>You can display an error</FormHelperText>
                    </FormControl>
                </Box>
            </MainCard>
        )
    );
};

export default SamplePage;
