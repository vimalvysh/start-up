import React from "react";
import {useDispatch} from "react-redux";

// material-ui
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import MenuItem from "@mui/material/MenuItem";
// project imports
import Stack from "@mui/material/Stack";
import MainCard from "ui-component/cards/MainCard";
import Button from "@mui/material/Button";
// Icons.
import SearchIcon from "@mui/icons-material/Search";

// Actions.
import {showSearch, searchRequested} from "../../../../store/actions/searchAction";

// Component.
import MoreFilter from "./MoreFilter";

const SamplePage = () => {
    const dispatch = useDispatch();
    const [isShowFilters, setIsShowFilters] = React.useState(false);
    const [values, setValues] = React.useState({
        amount: "",
        password: "",
        weight: "",
        weightRange: "",
        showPassword: false
    });

    const handleChange = (prop) => (event) => {
        setValues({...values, [prop]: event.target.value});
    };

    const handleToggleMoreFilter = () => {
        try {
            setIsShowFilters((prevState) => !prevState);
        } catch (error) {
            console.log(error);
        }
    };

    // To close the search page.
    const handleClose = () => {
        try {
            // call redux to make false, showing search.
            dispatch(showSearch(false));
            // call redux to make search value "".
            dispatch(searchRequested(""));
        } catch (error) {
            console.log(error);
        }
    };
    return (
        <MainCard>
            <MoreFilter isShowFiletrs={isShowFilters} handleToggleMoreFilter={handleToggleMoreFilter} />
            <Stack spacing={2} direction="row">
                {/* <Box sx={{display: "flex", alignItems: "flex-end"}}>
                    <TextField
                        id="input-with-sx"
                        sx={{width: 400}}
                        variant="outlined"
                        size="small"
                        placeholder="Search by department, company, skills "
                    />
                </Box> */}
                <TextField id="outlined-select-currency" select value={"Sector"} size="small" sx={{width: 250}} onChange={handleChange}>
                    <MenuItem key={1} value={"Sector"}>
                        {"Sector"}
                    </MenuItem>
                </TextField>
                <TextField id="outlined-select-currency" select value={"Department"} size="small" sx={{width: 250}} onChange={handleChange}>
                    <MenuItem key={1} value={"Department"}>
                        {"Department"}
                    </MenuItem>
                </TextField>
                <TextField id="outlined-select-currency" select value={"Skills"} size="small" sx={{width: 250}} onChange={handleChange}>
                    <MenuItem key={1} value={"Skills"}>
                        {"Skills"}
                    </MenuItem>
                </TextField>
                <Button variant="outlined" sx={{width: 200, alignItems: "right"}} onClick={handleToggleMoreFilter}>
                    More Filters
                </Button>
                <Button variant="outlined" sx={{width: 100, alignItems: "right"}} onClick={handleClose}>
                    X
                </Button>
            </Stack>
        </MainCard>
    );
};

export default SamplePage;
