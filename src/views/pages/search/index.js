import React from "react";

// Components.
import SearchBar from "./components/SearchBar";
import UserContainer from "./components/UserContainer";

const details = [
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Tom",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Henderson",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Jerry",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Spike",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Donald",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: "#4Friends_Henderson",
        avatar: `img-user.png`,
        name: "Micky",
        location: "South Antonina",
        designation: "Software Engineer",
        company: "TCS",
        description: ""
    }
];

const SamplePage = () => (
    <>
        <SearchBar />
        <UserContainer details={details} />
    </>
);

export default SamplePage;
