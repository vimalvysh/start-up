import React, {useEffect, useState} from "react";

// material-ui
import {Grid} from "@material-ui/core";
import {gridSpacing} from "store/constant";

// Services.
import {getUserDatas} from "./services";

// Components.
import UserDetails from "./components/UserDetails";
import About from "./components/About";
import Education from "./components/Education";
import WorkExperience from "./components/WorkExperience";
import Jobs from "./components/Jobs";
import EmployeeBenefits from "./components/EmployeeBenefits";

// import Skills from "../";

import Calendar from "../../application/calendar";

// Data.
import Data from "./dummyData";

// ===========================|| DEFAULT DASHBOARD ||=========================== //

const UserProfile = () => {
    const {workExperience, topMembers} = Data;
    const USER = JSON.parse(localStorage.getItem("user-token"));
    const [isLoading, setLoading] = useState(true);
    const [userDatas, setUserDatas] = useState(null);
    useEffect(() => {
        setLoading(false);
    }, []);

    useEffect(async () => {
        let userId = USER.id;
        let response = await getUserDatas(userId);
        setLoading(true);
        if (response.status === 200) {
            setUserDatas(response.data);
            setLoading(false);
        }
        return () => {
            setUserDatas(null);
        };
    }, []);

    return (
        !isLoading &&
        userDatas && (
            <Grid container spacing={gridSpacing}>
                <Grid item xs={6}>
                    <UserDetails details={userDatas?.user_details ?? null} />
                    <About data={userDatas?.user_details?.intro ?? ""} />
                    <Education data={topMembers ?? []} />
                    <WorkExperience data={userDatas?.experiences ?? []} />
                </Grid>
                <Grid item xs={6}>
                    <Jobs />
                    <EmployeeBenefits />
                </Grid>
            </Grid>
        )
    );
};

export default UserProfile;
