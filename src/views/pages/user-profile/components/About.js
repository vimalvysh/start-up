import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

// material-ui
import {makeStyles, useTheme} from "@material-ui/styles";
import {Button, Divider, Grid, IconButton, Link, TextField, Typography} from "@material-ui/core";
import {gridSpacing} from "store/constant";
import Box from "@mui/material/Box";

// assets
import EditIcon from "@mui/icons-material/Edit";

import AttachmentTwoToneIcon from "@material-ui/icons/AttachmentTwoTone";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LayersTwoToneIcon from "@material-ui/icons/LayersTwoTone";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import NavigateNextRoundedIcon from "@material-ui/icons/NavigateNextRounded";
import PeopleAltTwoToneIcon from "@material-ui/icons/PeopleAltTwoTone";
import PublicTwoToneIcon from "@material-ui/icons/PublicTwoTone";
import RecentActorsTwoToneIcon from "@material-ui/icons/RecentActorsTwoTone";

// Components.
import MainCard from "ui-component/cards/MainCard";

// style constant
const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    link: {
        color: "blue"
    }
}));
// ===========================|| DEFAULT DASHBOARD ||=========================== //

const UserProfile = ({data}) => {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} className={classes.aboutContainer}>
                <MainCard
                    title={
                        <Grid container spacing={0}>
                            <Grid item xs zeroMinWidth>
                                <Typography variant="h4">About</Typography>
                            </Grid>
                            <Grid item>
                                <EditIcon
                                    fontSize="small"
                                    className={classes.primaryLight}
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                />
                            </Grid>
                        </Grid>
                    }
                >
                    <div style={{width: "100%", fontSize: ".756rem"}}>
                        <Box sx={{display: "flex", p: 1}}>
                            <Box sx={{width: "50%"}}>Website</Box>
                            <Box sx={{flexGrow: 1}}>www.livoMed.com</Box>
                        </Box>
                        <Box sx={{display: "flex", p: 1}}>
                            <Box sx={{width: "50%"}}>Founded</Box>
                            <Box sx={{flexGrow: 1}}>2016</Box>
                        </Box>
                        <Box sx={{display: "flex", p: 1, bgcolor: "background.paper"}}>
                            <Box sx={{width: "50%"}}>Employee</Box>
                            <Box sx={{flexGrow: 1}}>11 - 15</Box>
                        </Box>
                        <Box sx={{display: "flex", p: 1, bgcolor: "background.paper"}}>
                            <Box sx={{width: "50%"}}>Industries</Box>
                            <Box sx={{flexGrow: 1}}>Pharma, Helthcare, Life Science</Box>
                        </Box>
                        <Box sx={{display: "flex", p: 1, bgcolor: "background.paper"}}>
                            <Box sx={{width: "50%"}}>Funding state</Box>
                            <Box sx={{flexGrow: 1}}>Pre-Seed</Box>
                        </Box>
                    </div>

                    <Box sx={{p: 1, fontSize: ".756rem"}}>
                        {`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a
                        type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                        Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.`.substring(0, 400)}
                        ...
                        <Grid item xs={12}>
                            <Typography align="left" variant="caption" className={classes.link}>
                                Read more
                            </Typography>
                        </Grid>
                    </Box>
                </MainCard>
            </Grid>
        </Grid>
    );
};

UserProfile.PropTypes = {
    data: PropTypes.string
};

export default UserProfile;
