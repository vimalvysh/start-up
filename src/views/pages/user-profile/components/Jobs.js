import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

// material-ui
import {makeStyles, useTheme} from "@material-ui/styles";
import {Button, Divider, Grid, IconButton, Link, TextField, Typography} from "@material-ui/core";
import {gridSpacing} from "store/constant";
import Box from "@mui/material/Box";

// assets
import EditIcon from "@mui/icons-material/Edit";

import AttachmentTwoToneIcon from "@material-ui/icons/AttachmentTwoTone";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LayersTwoToneIcon from "@material-ui/icons/LayersTwoTone";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import NavigateNextRoundedIcon from "@material-ui/icons/NavigateNextRounded";
import PeopleAltTwoToneIcon from "@material-ui/icons/PeopleAltTwoTone";
import PublicTwoToneIcon from "@material-ui/icons/PublicTwoTone";
import RecentActorsTwoToneIcon from "@material-ui/icons/RecentActorsTwoTone";
import BusinessCenterIcon from "@mui/icons-material/BusinessCenter";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import LocalAtmIcon from "@mui/icons-material/LocalAtm";

// Components.
import MainCard from "ui-component/cards/MainCard";

// style constant
const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    link: {
        color: "blue"
    },
    aboutContainer: {
        marginTop: "8px"
    },
    skillContainer: {
        marginTop: "12px",
        "& button": {
            backgroundColor: "#d1eef0",
            borderRadius: "20px",
            color: "#53a0ed",
            fontSize: ".546rem"
        }
    }
}));

const Jobs = ({data}) => {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} className={classes.aboutContainer}>
                <MainCard
                    title={
                        <Grid container spacing={0}>
                            <Grid item xs zeroMinWidth>
                                <Typography variant="h4">Jobs at LiveMed</Typography>
                            </Grid>
                            <Grid item>
                                <Button variant="contained">
                                    <Typography
                                        variant="p"
                                        sx={{
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",

                                            display: "block",
                                            fontSize: ".576rem"
                                        }}
                                    >
                                        Post Job
                                    </Typography>
                                </Button>
                            </Grid>
                        </Grid>
                    }
                >
                    <div style={{width: "100%", fontSize: ".756rem"}}>
                        <Box sx={{display: "flex"}}>Social Media Manager</Box>
                        <Box sx={{display: "flex"}}>
                            <Box sx={{flexGrow: 1}}>
                                <BusinessCenterIcon
                                    fontSize="small"
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                    sx={{marginRight: "4px", fontSize: ".865rem"}}
                                />
                                Managmennt
                            </Box>
                            <Box sx={{flexGrow: 1}}>
                                <AddLocationIcon
                                    fontSize="small"
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                    sx={{marginRight: "4px", fontSize: ".865rem"}}
                                />
                                Abhu Bhabi
                            </Box>
                            <Box sx={{flexGrow: 1}}>
                                <LocalAtmIcon
                                    fontSize="small"
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                    sx={{marginRight: "4px", fontSize: ".865rem"}}
                                />
                                $15- $25 / hour
                            </Box>
                        </Box>
                    </div>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} className={classes.aboutContainer}>
                            <Grid container spacing={2}>
                                <Grid item className={classes.skillContainer}>
                                    <Button className={classes.skills} size="small" variant="contained" disableElevation>
                                        Nodejs
                                    </Button>
                                </Grid>
                                <Grid item className={classes.skillContainer}>
                                    <Button className={classes.skills} size="small" variant="contained" disableElevation>
                                        Javascript
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Box sx={{p: 1, fontSize: ".756rem"}}>
                        {`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a
                        type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                        Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.`.substring(0, 400)}
                        ...
                        <Grid item xs={12} sx={{marginTop: "10px"}}>
                            <Typography align="left" variant="caption" className={classes.link}>
                                See Details
                            </Typography>
                            <Button variant="outlined" sx={{float: "right"}}>
                                <Typography
                                    variant="p"
                                    sx={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",

                                        display: "block",
                                        fontSize: ".576rem"
                                    }}
                                >
                                    Apply Now
                                </Typography>
                            </Button>
                        </Grid>
                    </Box>
                </MainCard>
            </Grid>
        </Grid>
    );
};

Jobs.PropTypes = {
    data: PropTypes.string
};

export default Jobs;
