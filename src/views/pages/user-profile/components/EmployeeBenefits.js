import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

// material-ui
import {makeStyles, useTheme} from "@material-ui/styles";
import {Button, Divider, Grid, IconButton, Link, TextField, Typography} from "@material-ui/core";
import {gridSpacing} from "store/constant";
import Box from "@mui/material/Box";

// assets
import EditIcon from "@mui/icons-material/Edit";

import AttachmentTwoToneIcon from "@material-ui/icons/AttachmentTwoTone";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LayersTwoToneIcon from "@material-ui/icons/LayersTwoTone";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import NavigateNextRoundedIcon from "@material-ui/icons/NavigateNextRounded";
import PeopleAltTwoToneIcon from "@material-ui/icons/PeopleAltTwoTone";
import PublicTwoToneIcon from "@material-ui/icons/PublicTwoTone";
import RecentActorsTwoToneIcon from "@material-ui/icons/RecentActorsTwoTone";

// Components.
import MainCard from "ui-component/cards/MainCard";

// style constant
const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px"
    },
    description: {
        fontSize: ".676rem",
        marginBottom: "6px"
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    icon: {
        borderRadius: "6px",
        border: "1px solid #03cffc",
        padding: "8px",
        color: "#03cffc"
    }
}));
// ===========================|| DEFAULT DASHBOARD ||=========================== //

const UserProfile = ({data}) => {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} className={classes.aboutContainer}>
                <MainCard
                    title={
                        <Grid container spacing={0}>
                            <Grid item xs zeroMinWidth>
                                <Typography variant="h4">Employee Benefits</Typography>
                            </Grid>
                            <Grid item>
                                <EditIcon
                                    fontSize="small"
                                    className={classes.primaryLight}
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                />
                            </Grid>
                        </Grid>
                    }
                >
                    <div style={{width: "100%", fontSize: ".756rem"}}>
                        <Box sx={{display: "flex", p: 1}}>
                            <Box class={classes.icon}>
                                <AttachmentTwoToneIcon
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                    sx={{marginRight: "4px", marginTop: "4px", fontSize: "1.665rem"}}
                                />
                            </Box>
                            <Box sx={{marginLeft: "10px"}}>
                                <Typography variant="h6">Healthcare benefit</Typography>
                                <Typography variant="subtitle2">Medical/dental/health(90% permium paid by cmpany)</Typography>
                            </Box>
                        </Box>
                        <Box sx={{display: "flex", p: 1}}>
                            <Box class={classes.icon}>
                                <AttachmentTwoToneIcon
                                    aria-controls="menu-friend-card"
                                    aria-haspopup="true"
                                    sx={{marginRight: "4px", marginTop: "4px", fontSize: "1.665rem"}}
                                />
                            </Box>
                            <Box sx={{marginLeft: "10px"}}>
                                <Typography variant="h6">Comapny meals benefit</Typography>
                                <Typography variant="subtitle2">
                                    Free snaks, drinks and healthy food. Unlimited coffee to go alongwith.
                                </Typography>
                            </Box>
                        </Box>
                    </div>
                </MainCard>
            </Grid>
        </Grid>
    );
};

UserProfile.PropTypes = {
    data: PropTypes.string
};

export default UserProfile;
