import PropTypes from "prop-types";
import React from "react";
import {Link} from "react-router-dom";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Button, CardActions, CardContent, Divider, Grid, Typography} from "@material-ui/core";

// project imports
import Avatar from "ui-component/extended/Avatar";
import MainCard from "ui-component/cards/MainCard";
import {gridSpacing} from "store/constant";

// assets
import EditIcon from "@mui/icons-material/Edit";

// style constant
const useStyles = makeStyles((theme) => ({
    educationContainer: {
        marginTop: "8px",
        minHeight: "300px"
    },
    projectTableMain: {
        position: "relative",
        marginBottom: "18px",
        marginTop: "10px",
        "&>*": {
            position: "relative",
            zIndex: "5"
        },
        "&:after": {
            content: '""',
            position: "absolute",
            top: "10px",
            left: "25px",
            width: "2px",
            height: "100%",
            background: "#ebebeb",
            zIndex: "1"
        }
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    },
    designation: {
        fontWeight: "600",
        fontSize: ".765rem"
    },
    years: {
        fontWeight: "600",
        fontSize: ".765rem",
        color: "#2729b0"
    },
    company: {
        fontWeight: "600",
        fontSize: ".765rem",
        color: "#2729b0"
    },
    avatarIcon: {
        width: "20px",
        height: "20px",
        fontSize: ".656rem"
    }
}));

// ==============================|| DATA WIDGET - TASKS CARD ||============================== //

const WorkExperience = ({title = "Work & Experience", data}) => {
    const classes = useStyles();

    // const TitileContainer = () => (
    //     <Grid container spacing={2}>
    //         <Grid item xs zeroMinWidth>
    //             <Typography variant="h4">{title}</Typography>
    //         </Grid>
    //         <Grid item>
    //             <EditIcon fontSize="small" className={classes.primaryLight} aria-controls="menu-friend-card" aria-haspopup="true" />
    //         </Grid>
    //     </Grid>
    // );

    return (
        <MainCard
            title={
                <Grid container spacing={2}>
                    <Grid item xs zeroMinWidth>
                        <Typography variant="h4">Funding</Typography>
                    </Grid>
                    <Grid item>
                        <EditIcon fontSize="small" className={classes.primaryLight} aria-controls="menu-friend-card" aria-haspopup="true" />
                    </Grid>
                </Grid>
            }
            content={false}
            className={classes.educationContainer}
        >
            <CardContent>
                <Grid container spacing={0}>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                Amount Raised
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                $ 100,556
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                Found Over
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                2 Rounds
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                Total Validation
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align="left" variant="subtitle2" className={classes.designation}>
                                $ 100,556
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <div className={classes.line}></div>
                <Grid container spacing={gridSpacing} alignItems="center" className={classes.projectTableMain}>
                    {data.map((el, i) => (
                        <Grid key={el.id} item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <Avatar size="small" className={classes.avatarIcon}>
                                        {/* <ThumbUpAltOutlinedIcon /> */}
                                        {i + 1}
                                    </Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Grid container spacing={0}>
                                        <Grid container spacing={1}>
                                            <Grid item>
                                                <Typography align="left" variant="body2" className={classes.designation}>
                                                    $ 100,556
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography align="left" variant="caption" className={classes.company}>
                                                Pre Seed
                                            </Typography>
                                            <Typography align="left" variant="subtitle2">
                                                20 Jul 2019
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography align="left" variant="caption" className={classes.company}>
                                                Dilip Kumar
                                            </Typography>
                                            <Typography align="left" variant="subtitle2">
                                                Angle investor
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
            </CardContent>
        </MainCard>
    );
};

WorkExperience.propTypes = {
    title: PropTypes.string,
    data: PropTypes.array
};

export default WorkExperience;
