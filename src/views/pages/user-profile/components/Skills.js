import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

// material-ui
import {makeStyles, useTheme} from "@material-ui/styles";
import {Button, Divider, Grid, IconButton, Link, TextField, Typography} from "@material-ui/core";
import {gridSpacing} from "store/constant";

// assets

import EditIcon from "@mui/icons-material/Edit";
// Components.
import MainCard from "ui-component/cards/MainCard";

// style constant
const useStyles = makeStyles((theme) => ({
    aboutContainer: {
        marginTop: "8px"
    },
    skillContainer: {
        marginTop: "12px",
        "& button": {
            backgroundColor: "#d1eef0",
            borderRadius: "20px",
            color: "#53a0ed",
            fontSize: ".546rem"
        }
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    }
}));
// ===========================|| DEFAULT DASHBOARD ||=========================== //

const Skills = ({data}) => {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} className={classes.aboutContainer}>
                <MainCard>
                    <Grid container spacing={2}>
                        <Grid item xs zeroMinWidth>
                            <Typography variant="h4">Skills</Typography>
                        </Grid>
                        <Grid item>
                            <EditIcon
                                fontSize="small"
                                className={classes.primaryLight}
                                aria-controls="menu-friend-card"
                                aria-haspopup="true"
                            />
                        </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                        {data.map((el) => (
                            <Grid key={el.id} item className={classes.skillContainer}>
                                <Button className={classes.skills} size="small" variant="contained" disableElevation>
                                    {el.name}
                                </Button>
                            </Grid>
                        ))}
                    </Grid>
                </MainCard>
            </Grid>
        </Grid>
    );
};

Skills.propTypes = {
    data: PropTypes.array
};

export default Skills;
