import PropTypes from "prop-types";
import React from "react";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Avatar, Button, Card, Grid, ListItemIcon, Menu, MenuItem, Typography, Tooltip} from "@material-ui/core";
// project imports
import {gridSpacing} from "store/constant";

// assets
import WorkIcon from "@mui/icons-material/Work";
import MissedVideoCallIcon from "@mui/icons-material/MissedVideoCall";
import ForumIcon from "@mui/icons-material/Forum";

import MoreHorizOutlinedIcon from "@material-ui/icons/MoreHorizOutlined";
import PinDropTwoToneIcon from "@material-ui/icons/PinDropTwoTone";
import FavoriteTwoToneIcon from "@material-ui/icons/FavoriteTwoTone";
import DeleteTwoToneIcon from "@material-ui/icons/DeleteTwoTone";
import GroupTwoToneIcon from "@material-ui/icons/GroupTwoTone";
import EditIcon from "@mui/icons-material/Edit";

const avatarImage = require.context("assets/images/profile", true);

// style constant
const useStyles = makeStyles((theme) => ({
    followerBlock: {
        padding: "16px",
        background: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[50],
        border: "1px solid",
        borderColor: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[100],
        "&:hover": {
            border: `1px solid ${theme.palette.primary.main}`
        }
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer"
    },
    btnProfile: {
        width: "100%",
        borderRadius: "4px",

        background: "#fff",
        borderColor: "#EDE7F6",
        "&:hover": {
            borderColor: "transparent"
        },
        "& svg": {
            width: "20px",
            height: "20px"
        }
    },
    bgSecondary: {
        background: theme.palette.mode === "dark" ? theme.palette.dark.dark : "#fff",
        borderColor: theme.palette.mode === "dark" && theme.palette.dark.dark,
        "&:hover": {
            background: theme.palette.secondary.light
        }
    },
    bgPrimary: {
        background: theme.palette.mode === "dark" ? theme.palette.dark.dark : "#fff",
        borderColor: theme.palette.mode === "dark" && theme.palette.dark.dark,
        "&:hover": {
            background: theme.palette.primary.light
        }
    },
    profileImage: {
        width: "100px",
        height: "100px",
        backgroundColor: "transparent"
    },
    position: {
        borderRadius: "16px",
        height: "20px",
        fontSize: ".686rem",
        color: "#fff",
        marginLeft: "10px",
        marginBottom: "8px"
    },
    skillContainer: {
        marginTop: "12px",
        "& button": {
            backgroundColor: "#d1eef0",
            borderRadius: "20px",
            color: "#53a0ed",
            fontSize: ".546rem"
        }
    },
    editIcon: {
        color: theme.palette.primary[200],
        cursor: "pointer",
        fontSize: ".856rem"
    }
}));

const UserDetails = ({details: {location = "", first_name = "", last_name = "", designation = "Software Engineer"}}) => {
    const classes = useStyles();

    // const avatarProfile = avatarImage(`./${avatar}`).default;

    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Card className={classes.followerBlock}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item>
                            <Avatar className={classes.profileImage} alt="User 1" variant="rounded" />
                        </Grid>
                        <Grid item xs sx={{marginTop: "10px"}}>
                            <Typography
                                variant="h4"
                                sx={{overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", display: "block"}}
                            >
                                {`${first_name} ${last_name}`}
                            </Typography>
                            <Typography
                                variant="subtitle2"
                                sx={{mt: 0.5, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", display: "block"}}
                            >
                                Utlizing Gene Repare to Treat Diease
                            </Typography>
                            <Typography
                                variant="subtitle2"
                                sx={{mt: 0.5, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", display: "block"}}
                            >
                                <PinDropTwoToneIcon fontSize="inherit" sx={{mr: 0.5, fontSize: "0.875rem", verticalAlign: "text-top"}} />
                                {location}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <EditIcon fontSize="small" className={classes.editIcon} aria-controls="menu-friend-card" aria-haspopup="true" />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
};

UserDetails.propTypes = {
    avatar: PropTypes.string,
    location: PropTypes.string,
    name: PropTypes.string
};

export default UserDetails;
