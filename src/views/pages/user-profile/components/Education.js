import PropTypes from "prop-types";
import React from "react";
import {Link} from "react-router-dom";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {Button, Card, CardContent, Divider, Grid, Typography, Tooltip} from "@material-ui/core";

// project imports
import Avatar from "ui-component/extended/Avatar";
import MainCard from "ui-component/cards/MainCard";
import {gridSpacing} from "store/constant";

// assets
import EditIcon from "@mui/icons-material/Edit";
import PinDropTwoToneIcon from "@material-ui/icons/PinDropTwoTone";
import VideoCallTwoToneIcon from "@material-ui/icons/VideoCallTwoTone";
import ChatBubbleTwoToneIcon from "@material-ui/icons/ChatBubbleTwoTone";

const avatarImage = require.context("assets/images/profile", true);

// style constant
const useStyles = makeStyles((theme) => ({
    container: {
        // marginTop: "10px"
    },
    followerBlock: {
        padding: "16px",
        border: "1px solid",

        borderColor: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[100],
        "&:hover": {
            border: `1px solid ${theme.palette.primary.main}`
        }
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: "pointer"
    },
    btnProfile: {
        width: "100%",
        // paddingTop: "0px",
        padding: "4px",

        borderRadius: "4px",
        background: "#fff",
        borderColor: "#EDE7F6",
        "&:hover": {
            borderColor: "transparent"
        },
        "& svg": {
            width: "20px",
            height: "20px",
            marginRight: "2px"
        }
    },
    bgSecondary: {
        background: theme.palette.mode === "dark" ? theme.palette.dark.dark : "#fff",
        borderColor: theme.palette.mode === "dark" && theme.palette.dark.dark,
        "&:hover": {
            background: theme.palette.secondary.light
        }
    },
    bgPrimary: {
        background: theme.palette.mode === "dark" ? theme.palette.dark.dark : "#fff",
        borderColor: theme.palette.mode === "dark" && theme.palette.dark.dark,
        "&:hover": {
            background: theme.palette.primary.light
        }
    },
    profileImage: {
        width: "60px",
        height: "60px",
        backgroundColor: "transparent"
    },
    viewProfileBtn: {
        borderRadius: "10px"
    },
    link: {
        color: "blue"
    }
}));

// ==============================|| DATA WIDGET - TASKS CARD ||============================== //

const Education = ({title = "Educations", data}) => {
    const classes = useStyles();

    return (
        <MainCard
            title={
                <Grid container spacing={0}>
                    <Grid item xs zeroMinWidth>
                        <Typography variant="h4">{"Team Members"}</Typography>
                    </Grid>
                    <Grid item>
                        <EditIcon fontSize="small" className={classes.primaryLight} aria-controls="menu-friend-card" aria-haspopup="true" />
                    </Grid>
                </Grid>
            }
            content={false}
            className={classes.educationContainer}
        >
            <CardContent>
                <Grid container spacing={2} className={classes.container}>
                    {data.map((el, i) => (
                        <Grid item xs={6} md={6}>
                            <Card className={classes.followerBlock}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Grid container spacing={gridSpacing}>
                                            <Grid item>
                                                <Avatar
                                                    alt="User 1"
                                                    className={classes.profileImage}
                                                    src={el.avatar && avatarImage(`./${el.avatar}`).default}
                                                />
                                            </Grid>
                                            <Grid item xs zeroMinWidth sx={{marginTop: "10px"}}>
                                                <Typography
                                                    variant="h5"
                                                    sx={{
                                                        overflow: "hidden",
                                                        textOverflow: "ellipsis",
                                                        whiteSpace: "nowrap",
                                                        display: "block"
                                                    }}
                                                >
                                                    {el.name}
                                                </Typography>
                                                <Typography
                                                    variant="subtitle2"
                                                    sx={{
                                                        mt: 0.5,
                                                        textOverflow: "ellipsis",
                                                        whiteSpace: "nowrap",
                                                        display: "block"
                                                    }}
                                                >
                                                    {el.designation}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Grid container spacing={1} sx={{height: "20vh"}}>
                                            <Grid item xs={12}>
                                                {/* <Tooltip title="Video Call" placement="top"> */}
                                                <Typography
                                                    variant="subtitle2"
                                                    sx={{
                                                        mt: 0.5,

                                                        textOverflow: "ellipsis",

                                                        display: "block"
                                                    }}
                                                >
                                                    {el.description && el.description.substring(0, 160)}
                                                </Typography>
                                                {/* </Tooltip> */}
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={2} sx={{marginTop: "10px"}}>
                                            <Grid item xs={12}>
                                                <Button variant="outlined" color="secondary" className={classes.viewProfileBtn}>
                                                    <VideoCallTwoToneIcon />
                                                    <Typography
                                                        variant="p"
                                                        sx={{
                                                            overflow: "hidden",
                                                            textOverflow: "ellipsis",
                                                            display: "block",
                                                            fontSize: ".576rem"
                                                        }}
                                                    >
                                                        View Profile
                                                    </Typography>
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                <Grid item xs={12}>
                    <Typography align="left" variant="caption" className={classes.link}>
                        See All
                    </Typography>
                </Grid>
            </CardContent>
        </MainCard>
    );
};

Education.propTypes = {
    title: PropTypes.string,
    data: PropTypes.array
};

export default Education;
