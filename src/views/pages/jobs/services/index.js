import fetchInterceptor from "../../../../utils/fetchInterceptor";

//#1: generateToken
export const getUserDatas = async (userId) => {
    try {
        const {data, status} = await fetchInterceptor.get(`/cv/api/profile/${userId}`);
        console.log("data", data);
        return {
            data,
            status
        };
    } catch (error) {
        return {
            data: error,
            status: error.status
        };
    }
};
