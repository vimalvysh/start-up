import React, {useEffect, useState} from "react";

// material-ui
import {Grid} from "@material-ui/core";
import {gridSpacing} from "store/constant";

// Services.
import {getUserDatas} from "./services";

// Components.
import Jobs from "./components/Jobs";
import JobDetails from "./components/JobDetails";

const Index = () => {
    const USER = JSON.parse(localStorage.getItem("user-token"));
    const [isLoading, setLoading] = useState(true);
    const [userDatas, setUserDatas] = useState(null);
    useEffect(() => {
        setLoading(false);
    }, []);

    useEffect(async () => {
        let userId = USER.id;
        let response = await getUserDatas(userId);
        setLoading(true);
        if (response.status === 200) {
            setUserDatas(response.data);
            setLoading(false);
        }
        return () => {
            setUserDatas(null);
        };
    }, []);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={6}>
                <Jobs />
            </Grid>
            <Grid item xs={6}>
                <JobDetails />
            </Grid>
        </Grid>
    );
};

export default Index;
