import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {Link as RouterLink, Route, useNavigate} from "react-router-dom";
import jwt from "jwt-decode";

// material-ui
import {Divider, Grid, Stack, Typography, useMediaQuery, Button, Box} from "@material-ui/core";
import AnimateButton from "ui-component/extended/AnimateButton";

// Active directory - signin
import {useMsal} from "@azure/msal-react";
import {loginRequest} from "../../../../authConfig";

// project imports
import AuthWrapper1 from "../AuthWrapper1";
import AuthCardWrapper from "../AuthCardWrapper";
import Logo from "ui-component/Logo";
import AppRegister from "../firebase-forms/AppRegister";
import AuthFooter from "ui-component/cards/AuthFooter";
import BackgroundPattern1 from "ui-component/cards/BackgroundPattern1";
import AuthSlider from "ui-component/cards/AuthSlider";
import {makeStyles, useTheme} from "@material-ui/styles";
// import stateConfig from "../../../../store/stateConfig";

import {generateTokenCreator, validateUserCreator} from "../../../../store/actions/authActions";
// assets
import AuthSignupCard from "assets/images/auth/student-artwork-signup.svg";

// ===============================|| AUTH - REGISTER ||=============================== //
// style constant
const useStyles = makeStyles((theme) => ({
    authPurpleCard: {
        "&:after": {
            content: '""',
            position: "absolute",
            top: "45%",
            left: "35%",
            width: "260px",
            backgroundSize: "380px",
            height: "290px",
            backgroundImage: `url(${AuthSignupCard})`,
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            animation: "15s wings ease-in-out infinite",
            [theme.breakpoints.down("lg")]: {
                left: "25%",
                top: "50%"
            }
        }
        // '&:before': {
        //     content: '""',
        //     position: 'absolute',
        //     top: '12%',
        //     left: '25%',
        //     width: '360px',
        //     height: '350px',
        //     backgroundSize: '460px',
        //     backgroundImage: `url(${AuthBlueCard})`,
        //     backgroundRepeat: 'no-repeat',
        //     backgroundPosition: 'center',
        //     animation: '15s wings ease-in-out infinite',
        //     animationDelay: '1s',
        //     [theme.breakpoints.down('lg')]: {
        //         top: '10%',
        //         left: '15%'
        //     }
        // }
    }
}));

// carousel items
const items = [
    {
        title: "JUSOOR Platform",
        description: "Educational Platform for Student, Mentors and Professionals"
    },
    {
        title: "JUSOOR Platform",
        description: "Educational Platform for Student, Mentors and Professionals"
    },
    {
        title: "JUSOOR Platform",
        description: "Educational Platform for Student, Mentors and Professionals"
    }
];

// function handleLogin(instance) {
//     instance.loginRedirect(loginRequest).catch(e => {
//         console.error(e);
//     });
// }

function handleLogin() {
    window.location.href =
        "https://jusoorlms.b2clogin.com/jusoorlms.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_useronboarding&client_id=93bb452a-a519-4d36-93db-29909b0ad2d4&nonce=defaultNonce&redirect_uri=http%3A%2F%2Flocalhost%3A3000&scope=openid&response_type=id_token&prompt=login";
}

function handleSignup() {
    window.location.href =
        "https://jusoorlms.b2clogin.com/jusoorlms.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_useronboarding&client_id=93bb452a-a519-4d36-93db-29909b0ad2d4&nonce=defaultNonce&redirect_uri=http%3A%2F%2Flocalhost%3A3000&scope=openid&response_type=id_token&prompt=login";
}

export const decodeToken = (tokenName, encodeToken) => {
    const decoded = jwt(encodeToken); // decode your token here
    localStorage.setItem(tokenName, JSON.stringify(decoded));
    return decoded;
};

const Register = (props) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const theme = useTheme();
    const {instance} = useMsal();
    const matchDownSM = useMediaQuery(theme.breakpoints.down("sm"));
    let IdToken = null;

    useEffect(() => {
        if (location.hash) {
            IdToken = location.hash.split("=")[1];
            localStorage.setItem("id_token", IdToken);
        }
    }, [location.hash]);

    useEffect(() => {
        dispatch(generateTokenCreator());
    }, []);

    useEffect(() => {
        if (IdToken) {
            let decodeTokenInfo = decodeToken("decode-token", IdToken);
            if (decodeTokenInfo) {
                // return <Navigate to="/onboard" />;
                navigate("/profile");
            }
        }
    }, [IdToken]);

    return (
        <AuthWrapper1>
            <Grid container justifyContent="space-between" alignItems="center" sx={{minHeight: "100vh"}}>
                <Grid item container justifyContent="center" md={6} lg={7} sx={{my: 3}}>
                    <Grid container justifyContent="center" alignItems="center" sx={{minHeight: "calc(100vh - 68px)"}}>
                        <Grid item sx={{m: {xs: 1, sm: 3}, mb: 0}}>
                            <AuthCardWrapper>
                                <Grid container spacing={2} alignItems="center" justifyContent="center">
                                    <Grid item xs={12}>
                                        <Grid
                                            container
                                            direction={matchDownSM ? "column-reverse" : "row"}
                                            alignItems="center"
                                            justifyContent="center"
                                        >
                                            <Grid item>
                                                <Stack alignItems="center" justifyContent="center" spacing={1}>
                                                    <Typography color="primary" gutterBottom variant={matchDownSM ? "h3" : "h2"}>
                                                        JUSOOR
                                                    </Typography>
                                                    <Typography variant="caption" fontSize="16px" textAlign={matchDownSM ? "center" : ""}>
                                                        {/* Enter your details to continue */}
                                                        Start your journey
                                                    </Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12}>
                                        {/* <AppRegister /> */}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box
                                            sx={{
                                                mt: 2
                                            }}
                                        >
                                            <AnimateButton>
                                                <Button
                                                    disableElevation
                                                    fullWidth
                                                    size="large"
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={() => handleSignup()}
                                                >
                                                    Sign up
                                                </Button>
                                            </AnimateButton>
                                        </Box>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={12}
                                        sx={{
                                            mt: 2
                                        }}
                                    >
                                        <Divider />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Grid item container direction="column" alignItems="center" xs={12}>
                                            <Typography
                                                component={RouterLink}
                                                to="/pages/login/login3"
                                                variant="subtitle1"
                                                sx={{textDecoration: "none"}}
                                            >
                                                Have an account?
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box
                                            sx={{
                                                mt: 2
                                            }}
                                        >
                                            <AnimateButton>
                                                {/* <Button
                                                    disableElevation
                                                    // disabled={isSubmitting}
                                                    fullWidth
                                                    size="large"
                                                    type="submit"
                                                    variant="outlined"
                                                    color="primary"
                                                    onClick={() => handleLogin(instance)}
                                                >
                                                    Sign in
                                                </Button> */}

                                                <Button
                                                    disableElevation
                                                    fullWidth
                                                    size="large"
                                                    type="submit"
                                                    variant="outlined"
                                                    color="primary"
                                                    onClick={() => handleLogin()}
                                                >
                                                    Sign in
                                                </Button>
                                            </AnimateButton>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </AuthCardWrapper>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={6} lg={5} sx={{position: "relative", alignSelf: "stretch", display: {xs: "none", md: "block"}}}>
                    <BackgroundPattern1>
                        <Grid item container alignItems="flex-end" justifyContent="center" spacing={3}>
                            <Grid item xs={12}>
                                <span />
                                <span className={classes.authPurpleCard} />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid item container justifyContent="center" sx={{pb: 8}}>
                                    <Grid item xs={10} lg={8} sx={{"& .slick-list": {pb: 2}}}>
                                        <AuthSlider items={items} />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </BackgroundPattern1>
                </Grid>
            </Grid>
        </AuthWrapper1>
    );
};

export default Register;
