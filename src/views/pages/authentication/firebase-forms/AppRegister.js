import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {registerCreator} from "../../../../store/actions/onboardUserActions";
import LoadingBox from "../../components/LoadingBox";
import MessageBox from "../../components/MessageBox";
import {Navigate} from "react-router-dom";

// material-ui
import {makeStyles} from "@material-ui/styles";
import {
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    useMediaQuery,
    FormLabel,
    Radio,
    RadioGroup,
    Paper
} from "@material-ui/core";

// third party
import {Formik} from "formik";
import * as Yup from "yup";

// project imports
import useAuth from "hooks/useAuth";
import useScriptRef from "hooks/useScriptRef";
import Google from "assets/images/icons/social-google.svg";
import AnimateButton from "ui-component/extended/AnimateButton";
import stateConfig from "store/stateConfig";
// import {strengthColor, strengthIndicator} from "utils/password-strength";

// assets
// import Visibility from "@material-ui/icons/Visibility";
// import VisibilityOff from "@material-ui/icons/VisibilityOff";
// import { margin } from "@material-ui/system";

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: "1rem",
        fontWeight: 500,
        backgroundColor: theme.palette.mode === "dark" ? theme.palette.dark.main : theme.palette.grey[50],
        border: "1px solid",
        borderColor: theme.palette.mode === "dark" ? theme.palette.dark.light + 20 : theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: "none",
        "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? theme.palette.dark.light + 20 : theme.palette.primary.light
        },
        [theme.breakpoints.down("sm")]: {
            fontSize: "0.875rem"
        }
    },
    signDivider: {
        flexGrow: 1
    },
    paper: {
        padding: "12px",
        margin: "12px 0"
    },
    signText: {
        cursor: "unset",
        margin: theme.spacing(2),
        padding: "5px 56px",
        borderColor:
            theme.palette.mode === "dark" ? `${theme.palette.dark.light + 20} !important` : `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]} !important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: "16px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "8px"
        }
    },
    loginInput: {
        ...theme.typography.customInput
    }
}));

// ===========================|| REGISTER ||=========================== //

const AppRegister = ({loginIndex, ...others}) => {
    const classes = useStyles();
    const scriptedRef = useScriptRef();
    const matchDownSM = useMediaQuery((theme) => theme.breakpoints.down("sm"));
    const customization = useSelector((state) => state.customization);
    const dispatch = useDispatch();
    const [toOnboard, setToOnboard] = useState(false);

    const {firebaseEmailPasswordSignIn, firebaseGoogleSignIn} = useAuth();

    if (toOnboard === true) {
        return <Navigate to="/onboard/onboardstep1" />;
    }

    const googleHandler = async () => {
        try {
            await firebaseGoogleSignIn();
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <>
            <Grid container direction="column" justifyContent="center" spacing={2}>
                <Grid item xs={12}>
                    <AnimateButton>
                        <Button
                            disableElevation
                            fullWidth
                            className={classes.redButton}
                            onClick={googleHandler}
                            size="large"
                            variant="contained"
                        >
                            <img src={Google} alt="google" width="20px" sx={{mr: {xs: 1, sm: 2}}} className={classes.loginIcon} /> Sign up
                            with Google
                        </Button>
                    </AnimateButton>
                </Grid>
                <Grid item xs={12}>
                    <Box
                        sx={{
                            alignItems: "center",
                            display: "flex"
                        }}
                    >
                        <Divider className={classes.signDivider} orientation="horizontal" />
                        <AnimateButton>
                            <Button
                                variant="outlined"
                                className={classes.signText}
                                sx={{borderRadius: `${customization.borderRadius}px`}}
                                disableRipple
                                disabled
                            >
                                JUSOOR
                            </Button>
                        </AnimateButton>
                        <Divider className={classes.signDivider} orientation="horizontal" />
                    </Box>
                </Grid>
                <Grid item xs={12} container alignItems="center" justifyContent="center">
                    <Box
                        sx={{
                            mb: 2
                        }}
                    >
                        <Typography variant="subtitle1">Sign up with Email address</Typography>
                    </Box>
                </Grid>
            </Grid>

            <Formik
                initialValues={{
                    applicationId: 0,
                    userUuid: "199ecba4-2320-41b5-8730-8b7e5d071512",
                    firstName: "",
                    lastName: "",
                    emailAddress: "",
                    location: "",
                    gender: "M",
                    major: "",
                    jobTitle: "",
                    currentCompany: "",
                    linkedinLink: null,
                    reachableBy: null,
                    intentionId: 0
                }}
                validationSchema={Yup.object().shape({
                    firstName: Yup.string().required("First name is required"),
                    lastName: Yup.string().required("Last name is required"),
                    emailAddress: Yup.string().required("Email is required"),
                    location: Yup.string().required("Country is required"),
                    gender: Yup.string().required("Gender is required"),
                    major: Yup.string().required("Major is required"),
                    jobTitle: Yup.string().required("Job title is required"),
                    currentCompany: Yup.string().required("Current company is required")
                })}
                onSubmit={async (values, {setErrors, setStatus, setSubmitting}) => {
                    try {
                        console.log("Signup data: ", values);

                        // dispatch(registerCreator(values));
                        setToOnboard(true);
                    } catch (err) {
                        console.error(err);
                    }
                }}
            >
                {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, setFieldValue}) => (
                    <form noValidate onSubmit={handleSubmit} {...others}>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={6}>
                                <FormControl
                                    fullWidth
                                    error={Boolean(touched.firstName && errors.firstName)}
                                    className={classes.loginInput}
                                >
                                    <InputLabel htmlFor="outlined-adornment-fname-register">First Name</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-fname-register"
                                        type="text"
                                        value={values.firstName}
                                        name="firstName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.firstName && errors.firstName && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.firstName}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.lastName && errors.lastName)} className={classes.loginInput}>
                                    <InputLabel htmlFor="outlined-adornment-lname-register">Last Name</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-lname-register"
                                        type="text"
                                        value={values.lastName}
                                        name="lastName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.lastName && errors.lastName && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.lastName}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={12}>
                                <FormControl
                                    fullWidth
                                    error={Boolean(touched.emailAddress && errors.emailAddress)}
                                    className={classes.loginInput}
                                >
                                    <InputLabel htmlFor="outlined-adornment-email-register">Email Address / Username</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-email-register"
                                        type="emailAddress"
                                        value={values.emailAddress}
                                        name="emailAddress"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.emailAddress && errors.emailAddress && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.emailAddress}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                        </Grid>

                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={12}>
                                <Paper variant="outlined" className={classes.paper}>
                                    <FormControl component="fieldset">
                                        <FormLabel component="legend">Gender</FormLabel>
                                        <RadioGroup
                                            row
                                            aria-label="gender"
                                            name="gender"
                                            value={values.gender}
                                            onChange={(event) => {
                                                setFieldValue("gender", event.currentTarget.value);
                                            }}
                                        >
                                            <FormControlLabel value="F" control={<Radio />} label="Female" />
                                            <FormControlLabel value="M" control={<Radio />} label="Male" />
                                            <FormControlLabel value="O" control={<Radio />} label="Other" />
                                        </RadioGroup>
                                    </FormControl>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.location && errors.location)} className={classes.loginInput}>
                                    <InputLabel htmlFor="outlined-adornment-location-register">Location</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-location-register"
                                        type="text"
                                        value={values.location}
                                        name="location"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.location && errors.location && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.location}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.major && errors.major)} className={classes.loginInput}>
                                    <InputLabel htmlFor="outlined-adornment-major-register">Major</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-major-register"
                                        type="text"
                                        value={values.major}
                                        name="major"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.major && errors.major && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.major}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={6}>
                                <FormControl
                                    fullWidth
                                    error={Boolean(touched.currentCompany && errors.currentCompany)}
                                    className={classes.loginInput}
                                >
                                    <InputLabel htmlFor="outlined-adornment-currentCompany-register">Current Company</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-currentCompany-register"
                                        type="text"
                                        value={values.currentCompany}
                                        name="currentCompany"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.currentCompany && errors.currentCompany && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.currentCompany}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.jobTitle && errors.jobTitle)} className={classes.loginInput}>
                                    <InputLabel htmlFor="outlined-adornment-jobtitle-register">Job Title</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-jobtitle-register"
                                        type="text"
                                        value={values.jobTitle}
                                        name="jobTitle"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        inputProps={{
                                            classes: {
                                                notchedOutline: classes.notchedOutline
                                            }
                                        }}
                                    />
                                    {touched.jobTitle && errors.jobTitle && (
                                        <FormHelperText error id="standard-weight-helper-text--register">
                                            {" "}
                                            {errors.jobTitle}{" "}
                                        </FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                        </Grid>

                        {errors.submit && (
                            <Box
                                sx={{
                                    mt: 3
                                }}
                            >
                                <FormHelperText error>{errors.submit}</FormHelperText>
                            </Box>
                        )}

                        <Box
                            sx={{
                                mt: 2
                            }}
                        >
                            <AnimateButton>
                                <Button
                                    disableElevation
                                    // disabled={isSubmitting}
                                    fullWidth
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                >
                                    Sign up
                                </Button>
                            </AnimateButton>
                        </Box>
                    </form>
                )}
            </Formik>
        </>
    );
};

export default AppRegister;
