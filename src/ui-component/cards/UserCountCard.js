import PropTypes from 'prop-types';
import React from 'react';

// material-ui
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Button } from '@material-ui/core';
// assets
import LayersTwoToneIcon from '@material-ui/icons/LayersTwoTone';
import AddAlarmTwoToneIcon from '@material-ui/icons/AddAlarmTwoTone';

// style constant
const useStyles = makeStyles({
    revenueCard: {
        position: 'relative',
        color: '#fff'
    },
    revenueIcon: {
        position: 'absolute',
        left: '-17px',
        bottom: '-27px',
        color: '#fff',
        transform: 'rotate(25deg)',
        '&> svg': {
            width: '100px',
            height: '100px',
            opacity: '0.35'
        }
    },
    spanStyle: {
        fontSize: '12px',
        color: '#90CAF9'
    },
    btnStyle:{
        backgroundColor: '#E3F2FD',
        color: '#2196F3',
        '&:hover': {
            backgroundColor: '#F18362',
            color: '#fff'
        }
    }    
});

// =============================|| USER NUM CARD ||============================= //

const UserCountCard = ({ primary, secondary, iconPrimary, color }) => {
    const classes = useStyles();

    const IconPrimary = iconPrimary;
    const primaryIcon = iconPrimary ? <IconPrimary fontSize="large" /> : null;

    return (
        <Card style={{ background: '#1E88E5' }} className={classes.revenueCard}>
            <CardContent>
                <Typography variant="subtitle2" className={classes.revenueIcon}>
                    {primaryIcon}
                </Typography>
                <Grid container direction="column" justifyContent="center" alignItems="center" spacing={3}>
                    <Grid item sm={12}>
                        <Typography variant="h6" align="center" className={classes.spanStyle}>
                            {primary}
                        </Typography>
                    </Grid>
                    <Grid item sm={12}>
                        <Typography variant="h4" align="center" color="inherit">
                            {secondary}
                        </Typography>
                    </Grid>
                    <Grid item sm={12}>
                        <Typography variant="h6" align="center" className={classes.spanStyle}>
                            {'Connect with Aspirants'}
                        </Typography>
                    </Grid>
                    <Grid item sm={12}>
                                <Button className={classes.btnStyle} variant="contained" startIcon={<AddAlarmTwoToneIcon />}>
                                    Set Time Slots
                                </Button>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

UserCountCard.propTypes = {
    primary: PropTypes.string,
    secondary: PropTypes.string,
    iconPrimary: PropTypes.object,
    color: PropTypes.string
};

export default UserCountCard;
