import React from "react";
import ReactDOM from "react-dom";

// third party
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";

// load mock apis
import "_mockApis";

// project imports
import {store, persister} from "store";
import * as serviceWorker from "serviceWorker";
// import stateConfig from './store/stateConfig';
import App from "App";

// active directory - signin
import {PublicClientApplication} from "@azure/msal-browser";
import {MsalProvider} from "@azure/msal-react";
import {msalConfig} from "./authConfig";

// style + assets
import "assets/scss/style.scss";

// ===========================|| REACT DOM RENDER  ||=========================== //

const msalInstance = new PublicClientApplication(msalConfig);

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persister}>
            <MsalProvider instance={msalInstance}>
                <BrowserRouter>
                    <App />
                </BrowserRouter>
            </MsalProvider>
        </PersistGate>
    </Provider>,
    document.getElementById("root")
);

serviceWorker.unregister();
