import React, {lazy} from "react";

// project imports
import MainLayout from "layout/MainLayout";
import MainLayoutOnboard from "layout/MainLayoutOnboard";
import MainLayoutDashboard from "layout/MainLayoutDashboard";
import Loadable from "ui-component/Loadable";
import AuthGuard from "utils/route-guard/AuthGuard";
import {IconTemperatureMinus} from "@tabler/icons";

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import("views/dashboard/Default")));

//Onboard process
const OnboardStep1 = Loadable(lazy(() => import("views/onboard/steps")));

// sample page routing
const SamplePage = Loadable(lazy(() => import("views/sample-page")));

import {BrowserRouter, Link, Outlet, useRoutes} from "react-router-dom";

// ===========================|| MAIN ROUTING ||=========================== //

const MainRoutes = {
    path: "/onboard",
    element: (
        <MainLayoutOnboard />
        // <MainLayoutDashboard />
    ),
    children: [
        //only non-onboarded users should be able to access this
        {
            path: "/",
            element: <OnboardStep1 />
        },
        {
            path: "/main-dashboard",
            element: <DashboardDefault />
        }
    ]
};

export default MainRoutes;
